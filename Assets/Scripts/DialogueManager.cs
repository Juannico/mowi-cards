﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    public GameObject dialoguePanel, dialogueBtn;
    public Text speech, pronunctiation;
    public GameObject[] dialogueTargets;
    public GameObject[] languagePanelElements;

    public AudioClip[] audiosSpanish;
    public string[] textosSpanish;
    public AudioClip[] audiosEnglish;
    public string[] textosEnglish;
    public AudioClip[] audiosFrench;
    public string[] textosFrench;
    public AudioClip[] audiosGerman;
    public string[] textosGerman;
    public AudioClip[] audiosChinese;
    public string[] textosChinese;
    public string[] textosPChinese;
    public AudioClip[] audiosPortugese;
    public string[] textosPortugese;
    public AudioClip[] audiosItalian;
    public string[] textosItalian;
    public AudioClip[] audiosArabic;
    public string[] textosArabic;
    public string[] textosPArabic;
    public AudioClip[] audiosKorean;
    public string[] textosKorean;
    public string[] textosPKorean;
    //bool isHolding;
    //public string textodprueba;

    public AudioSource dialogueAudio;
    //int wordID;
    int randomNumberToDisplay;
    public HUDManager hUDManager;
    // Start is called before the first frame update
    void Start()
    {
        //isHolding = false;
        dialoguePanel.SetActive(false);
        dialogueBtn.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i< dialogueTargets.Length; i++)
        {
            if (dialogueTargets[i].GetComponent<VuforiaDialogue>().thisMarkerAffecting)//si i es verdadero
            {//si al menos uno de los i (en este frame) es verdadero, isholding es verdadero
                Debug.Log("this button on");
                randomNumberToDisplay = i;
                dialogueBtn.SetActive(true);
                break;
            }
            else//si i es falso
            {//si todos los i dan falso, isholding es falso
                //Debug.Log("this button off");
                dialogueBtn.SetActive(false);
            }
        }
        //wordID = hUDManager.wordID;
    }

    public void Dialogue(string type)
    {
        switch (type)
        {
            case "begin":
                dialoguePanel.SetActive(true);
                //randomNumberToDisplay = Random.Range(0, dialogueTargets.Length);
                for (int i = 0; i < languagePanelElements.Length; i++)
                {
                    languagePanelElements[i].SetActive(false);
                }
                break;
            case "close":
                dialoguePanel.SetActive(false);
                for (int i = 0; i < languagePanelElements.Length; i++)
                {
                    languagePanelElements[i].SetActive(true);
                }
                break;
            case "spanish":
                pronunctiation.text = "";
                dialogueAudio.clip = audiosSpanish[randomNumberToDisplay];
                speech.text = Transcribir(textosSpanish[randomNumberToDisplay]);
                //speech.text = textodprueba;
                break;
            case "english":
                pronunctiation.text = "";
                dialogueAudio.clip = audiosEnglish[randomNumberToDisplay];
                speech.text = Transcribir(textosEnglish[randomNumberToDisplay]);
                break;
            case "french":
                pronunctiation.text = "";
                dialogueAudio.clip = audiosFrench[randomNumberToDisplay];
                speech.text = Transcribir(textosFrench[randomNumberToDisplay]);
                break;
            case "german":
                pronunctiation.text = "";
                dialogueAudio.clip = audiosGerman[randomNumberToDisplay];
                speech.text = Transcribir(textosGerman[randomNumberToDisplay]);
                break;
            case "italian":
                pronunctiation.text = "";
                dialogueAudio.clip = audiosItalian[randomNumberToDisplay];
                speech.text = Transcribir(textosItalian[randomNumberToDisplay]);
                break;
            case "portuguese":
                pronunctiation.text = "";
                dialogueAudio.clip = audiosPortugese[randomNumberToDisplay];
                speech.text = Transcribir(textosPortugese[randomNumberToDisplay]);
                break;
            case "chinese":
                pronunctiation.text = Transcribir(textosPChinese[randomNumberToDisplay]);
                dialogueAudio.clip = audiosChinese[randomNumberToDisplay];
                speech.text = Transcribir(textosChinese[randomNumberToDisplay]);
                break;
            case "korean":
                pronunctiation.text = Transcribir(textosPKorean[randomNumberToDisplay]);
                dialogueAudio.clip = audiosKorean[randomNumberToDisplay];
                speech.text = Transcribir(textosKorean[randomNumberToDisplay]);
                break;
            case "arabic":
                pronunctiation.text = Transcribir(textosPArabic[randomNumberToDisplay]);
                dialogueAudio.clip = audiosArabic[randomNumberToDisplay];
                speech.text = Transcribir(textosArabic[randomNumberToDisplay]);
                break;
        }
        dialogueAudio.Play();
    }
    string Transcribir(string aux)
    {
        aux = aux.Replace("\\n", "\n");
        aux = aux.Replace("\\t", "\t");
        return aux;
    }
}
