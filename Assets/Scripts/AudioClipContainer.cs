﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioClipContainer : MonoBehaviour
{
    AudioSource voice;
    public AudioClip[] audiosFresa;
    public AudioClip[] audiosNaranja;
    public AudioClip[] audiosBanano;
    public AudioClip[] audiosLapiz;
    // Start is called before the first frame update
    void Start()
    {
        voice = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
