﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game3Script : MonoBehaviour
{
    public GameObject win, lose, gameover, container, playBtn, check1, check2, check3, check4, check5, check6;
    public Text instruccion, pronunciacion;
    int id, currentLanguage;
    int randomOrigin, randomTarget;
    public GameObject[] places;
    bool rollDice, buttonPressed;
    int rounds, origin1, target1;
    //int[] userInput;
    //public int[] track0, track1, track2, track3, track4, track5, track6, track7, track8, track9, track10, track11, track12, track13, track14, track15, track16, track17, track18, track19, track20, track21;
    //int[] track0 = new int [0, 7, 8, 1];
    // Start is called before the first frame update
    void Start()
    {
        rounds = 0; currentLanguage = 0;
        check1.SetActive(false); check2.SetActive(false); check3.SetActive(false); check4.SetActive(false); check5.SetActive(false); check6.SetActive(false);
        rollDice = false; buttonPressed = false;
        playBtn.SetActive(true);
        win.SetActive(false);
        origin1 = -1;
        target1 = -1;
        //container.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (rollDice)
        {
            if (randomOrigin == 0 && randomTarget == 1)
            {
                if (buttonPressed && id == 0 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 7 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 8 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 1 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true); buttonPressed = false; win.SetActive(true); rollDice = false;
                }
            }
            if (randomOrigin == 1 && randomTarget == 0)
            {
                if (buttonPressed && id == 1 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 8 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 7 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 0 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true); buttonPressed = false;win.SetActive(true); rollDice = false;
                }
            }
            else if (randomOrigin == 0 && randomTarget == 2)
            {
                if (buttonPressed && id == 0 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 7 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 8 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 2 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true); buttonPressed = false; win.SetActive(true); rollDice = false;
                }
            }
            else if (randomOrigin == 2 && randomTarget == 0)
            {
                if (buttonPressed && id == 2 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 8 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 7 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 0 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true); buttonPressed = false; win.SetActive(true); rollDice = false;
                }
            }
            else if (randomOrigin == 0 && randomTarget == 3)
            {
                if (buttonPressed && id == 0 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 7 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 8 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 11 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true);buttonPressed = false;
                }
                else if (buttonPressed && id == 3 && check1.activeSelf && check2.activeSelf && check3.activeSelf && check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check5.SetActive(true);buttonPressed = false; win.SetActive(true);rollDice = false;
                }
            }
            else if (randomOrigin == 3 && randomTarget == 0)
            {
                if (buttonPressed && id == 3 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 11 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 8 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 7 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 0 && check1.activeSelf && check2.activeSelf && check3.activeSelf && check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check5.SetActive(true); buttonPressed = false; win.SetActive(true); rollDice = false;
                }
            }
            else if (randomOrigin == 0 && randomTarget == 4)
            {
                if (buttonPressed && id == 0 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 7 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 8 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 11 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true);buttonPressed = false;
                }
                else if (buttonPressed && id == 4 && check1.activeSelf && check2.activeSelf && check3.activeSelf && check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check5.SetActive(true); buttonPressed = false; win.SetActive(true);rollDice = false;
                }
            }
            else if (randomOrigin == 4 && randomTarget == 0)
            {
                if (buttonPressed && id == 4 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 11 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 8 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 7 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 0 && check1.activeSelf && check2.activeSelf && check3.activeSelf && check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check5.SetActive(true); buttonPressed = false; win.SetActive(true); rollDice = false;
                }
            }
            else if (randomOrigin == 0 && randomTarget == 5)
            {
                if (buttonPressed && id == 0 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true);  buttonPressed = false;
                }
                else if (buttonPressed && id == 7 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 9 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true);buttonPressed = false;
                }
                else if (buttonPressed && id == 10 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 5 && check1.activeSelf && check2.activeSelf && check3.activeSelf && check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check5.SetActive(true); buttonPressed = false; win.SetActive(true);rollDice = false;
                }
            }
            else if (randomOrigin == 5 && randomTarget == 0)
            {
                if (buttonPressed && id == 5 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 10 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 9 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 7 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 0 && check1.activeSelf && check2.activeSelf && check3.activeSelf && check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check5.SetActive(true); buttonPressed = false; win.SetActive(true); rollDice = false;
                }
            }
            else if (randomOrigin == 0 && randomTarget == 6)
            {
                if (buttonPressed && id == 0 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 7 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true);buttonPressed = false;
                }
                else if (buttonPressed && id == 9 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true);buttonPressed = false;
                }
                else if (buttonPressed && id == 6 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true);buttonPressed = false; win.SetActive(true);rollDice = false;
                }
            }
            else if (randomOrigin == 6 && randomTarget == 0)
            {
                if (buttonPressed && id == 6 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 9 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 7 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 0 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true); buttonPressed = false; win.SetActive(true); rollDice = false;
                }
            }
            else if (randomOrigin == 1 && randomTarget == 2)
            {
                if (buttonPressed && id == 1 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 8 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 2 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true);buttonPressed = false; win.SetActive(true);rollDice = false;
                }
            }
            else if (randomOrigin == 2 && randomTarget == 1)
            {
                if (buttonPressed && id == 2 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 8 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 1 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true); buttonPressed = false; win.SetActive(true); rollDice = false;
                }
            }
            else if (randomOrigin == 1 && randomTarget == 3)
            {
                if (buttonPressed && id == 1 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true);  buttonPressed = false;
                }
                else if (buttonPressed && id == 8 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true);buttonPressed = false;
                }
                else if (buttonPressed && id == 11 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true);buttonPressed = false;
                }
                else if (buttonPressed && id == 3 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true);buttonPressed = false; win.SetActive(true);rollDice = false;
                }
            }
            else if (randomOrigin == 3 && randomTarget == 1)
            {
                if (buttonPressed && id == 3 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 11 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 8 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 1 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true); buttonPressed = false; win.SetActive(true); rollDice = false;
                }
            }
            else if (randomOrigin == 1 && randomTarget == 4)
            {
                if (buttonPressed && id == 1 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 8 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true);buttonPressed = false;
                }
                else if (buttonPressed && id == 11 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true);buttonPressed = false;
                }
                else if (buttonPressed && id == 4 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true);buttonPressed = false; win.SetActive(true);rollDice = false;
                }
            }
            else if (randomOrigin == 4 && randomTarget == 1)
            {
                if (buttonPressed && id == 4 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 11 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 8 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 1 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true); buttonPressed = false; win.SetActive(true); rollDice = false;
                }
            }
            else if (randomOrigin == 1 && randomTarget == 5)
            {
                if (buttonPressed && id == 1 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 8 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 10 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 5 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true); buttonPressed = false; win.SetActive(true); rollDice = false;
                }
            }
            else if (randomOrigin == 5 && randomTarget == 1)
            {
                if (buttonPressed && id == 5 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 10 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 8 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 1 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true); buttonPressed = false; win.SetActive(true); rollDice = false;
                }
            }
            else if (randomOrigin == 1 && randomTarget == 6)
            {
                if (buttonPressed && id == 1 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 8 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 11 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true);buttonPressed = false;
                }
                else if (buttonPressed && id == 10 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true);buttonPressed = false;
                }
                else if (buttonPressed && id == 9 && check1.activeSelf && check2.activeSelf && check3.activeSelf && check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check5.SetActive(true);buttonPressed = false;
                }
                else if (buttonPressed && id == 6 && check1.activeSelf && check2.activeSelf && check3.activeSelf && check4.activeSelf && check5.activeSelf && !check6.activeSelf)
                {
                    check6.SetActive(true);buttonPressed = false; win.SetActive(true);rollDice = false;
                }
            }
            else if (randomOrigin == 6 && randomTarget == 1)
            {
                if (buttonPressed && id == 6 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 9 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 10 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 11 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 8 && check1.activeSelf && check2.activeSelf && check3.activeSelf && check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check5.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 1 && check1.activeSelf && check2.activeSelf && check3.activeSelf && check4.activeSelf && check5.activeSelf && !check6.activeSelf)
                {
                    check6.SetActive(true); buttonPressed = false; win.SetActive(true); rollDice = false;
                }
            }
            else if (randomOrigin == 2 && randomTarget == 3)
            {
                if (buttonPressed && id == 2 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true);  buttonPressed = false;
                }
                else if (buttonPressed && id == 8 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true);buttonPressed = false;
                }
                else if (buttonPressed && id == 11 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true);buttonPressed = false;
                }
                else if (buttonPressed && id == 3 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true);buttonPressed = false; win.SetActive(true);rollDice = false;
                }
            }
            else if (randomOrigin == 3 && randomTarget == 2)
            {
                if (buttonPressed && id == 3 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 11 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 8 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 2 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true); buttonPressed = false; win.SetActive(true); rollDice = false;
                }
            }
            else if (randomOrigin == 2 && randomTarget == 4)
            {
                if (buttonPressed && id == 2 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 8 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true);buttonPressed = false;
                }
                else if (buttonPressed && id == 11 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 4 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true); buttonPressed = false; win.SetActive(true);rollDice = false;
                }
            }
            else if (randomOrigin == 4 && randomTarget == 2)
            {
                if (buttonPressed && id == 4 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 11 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 8 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 2 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true); buttonPressed = false; win.SetActive(true); rollDice = false;
                }
            }
            else if (randomOrigin == 2 && randomTarget == 5)
            {
                if (buttonPressed && id == 2 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true);buttonPressed = false;
                }
                else if (buttonPressed && id == 8 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true);buttonPressed = false;
                }
                else if (buttonPressed && id == 9 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 5 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true);buttonPressed = false; win.SetActive(true); rollDice = false;
                }
            }
            else if (randomOrigin == 5 && randomTarget == 2)
            {
                if (buttonPressed && id == 5 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 9 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 8 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 2 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true); buttonPressed = false; win.SetActive(true); rollDice = false;
                }
            }
            else if (randomOrigin == 2 && randomTarget == 6)
            {
                if (buttonPressed && id == 2 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true);buttonPressed = false;
                }
                else if (buttonPressed && id == 8 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true);buttonPressed = false;
                }
                else if (buttonPressed && id == 10 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true);buttonPressed = false;
                }
                else if (buttonPressed && id == 9 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 6 && check1.activeSelf && check2.activeSelf && check3.activeSelf && check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check5.SetActive(true); buttonPressed = false; win.SetActive(true); rollDice = false;
                }
            }
            else if (randomOrigin == 6 && randomTarget == 2)
            {
                if (buttonPressed && id == 6 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 9 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 10 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 8 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 2 && check1.activeSelf && check2.activeSelf && check3.activeSelf && check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check5.SetActive(true); buttonPressed = false;win.SetActive(true); rollDice = false;
                }
            }
            else if (randomOrigin == 3 && randomTarget == 4)
            {
                if (buttonPressed && id == 3 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 11 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 4 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true);buttonPressed = false; win.SetActive(true);rollDice = false;
                }
            }
            else if (randomOrigin == 4 && randomTarget == 3)
            {
                if (buttonPressed && id == 4 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 11 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 3 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true); buttonPressed = false; win.SetActive(true); rollDice = false;
                }
            }
            else if (randomOrigin == 3 && randomTarget == 5)
            {
                if (buttonPressed && id == 3 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 11 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true);buttonPressed = false;
                }
                else if (buttonPressed && id == 10 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 5 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true);buttonPressed = false; win.SetActive(true);rollDice = false;
                }
            }
            else if (randomOrigin == 5 && randomTarget == 3)
            {
                if (buttonPressed && id == 5 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 10 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 11 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 3 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true); buttonPressed = false; win.SetActive(true); rollDice = false;
                }
            }
            else if (randomOrigin == 3 && randomTarget == 6)
            {
                if (buttonPressed && id == 3 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true);  buttonPressed = false;
                }
                else if (buttonPressed && id == 11 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true);buttonPressed = false;
                }
                else if (buttonPressed && id == 10 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true);buttonPressed = false;
                }
                else if (buttonPressed && id == 9 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 6 && check1.activeSelf && check2.activeSelf && check3.activeSelf && check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check5.SetActive(true); buttonPressed = false; win.SetActive(true);rollDice = false;
                }
            }
            else if (randomOrigin == 6 && randomTarget == 3)
            {
                if (buttonPressed && id == 6 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 9 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 10 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 11 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 3 && check1.activeSelf && check2.activeSelf && check3.activeSelf && check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check5.SetActive(true); buttonPressed = false;win.SetActive(true); rollDice = false;
                }
            }
            else if (randomOrigin == 4 && randomTarget == 5)
            {
                if (buttonPressed && id == 4 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 11 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true);buttonPressed = false;
                }
                else if (buttonPressed && id == 10 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 5 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true); buttonPressed = false;win.SetActive(true);  rollDice = false;
                }
            }
            else if (randomOrigin == 5 && randomTarget == 4)
            {
                if (buttonPressed && id == 5 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 10 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 11 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 4 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true); buttonPressed = false;win.SetActive(true); rollDice = false;
                }
            }
            else if (randomOrigin == 4 && randomTarget == 6)
            {
                if (buttonPressed && id == 4 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true);buttonPressed = false;
                }
                else if (buttonPressed && id == 11 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 10 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true);buttonPressed = false;
                }
                else if (buttonPressed && id == 9 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true);buttonPressed = false;
                }
                else if (buttonPressed && id == 6 && check1.activeSelf && check2.activeSelf && check3.activeSelf && check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check5.SetActive(true);buttonPressed = false;win.SetActive(true); rollDice = false;
                }
            }
            else if (randomOrigin == 6 && randomTarget == 4)
            {
                if (buttonPressed && id == 6 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 9 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 10 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 11 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 4 && check1.activeSelf && check2.activeSelf && check3.activeSelf && check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check5.SetActive(true); buttonPressed = false;win.SetActive(true); rollDice = false;
                }
            }
            else if (randomOrigin == 5 && randomTarget == 6)
            {
                if (buttonPressed && id == 5 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true);  buttonPressed = false;
                }
                else if (buttonPressed && id == 10 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true);buttonPressed = false;
                }
                else if (buttonPressed && id == 9 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true);buttonPressed = false;
                }
                else if (buttonPressed && id == 6 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true);buttonPressed = false;win.SetActive(true);rollDice = false;
                }
            }
            else if (randomOrigin == 6 && randomTarget == 5)
            {
                if (buttonPressed && id == 6 && !check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check1.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 9 && check1.activeSelf && !check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check2.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 10 && check1.activeSelf && check2.activeSelf && !check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check3.SetActive(true); buttonPressed = false;
                }
                else if (buttonPressed && id == 5 && check1.activeSelf && check2.activeSelf && check3.activeSelf && !check4.activeSelf && !check5.activeSelf && !check6.activeSelf)
                {
                    check4.SetActive(true); buttonPressed = false;win.SetActive(true); rollDice = false;
                }
            }
        }
    }

    void LanguageShowCase()
    {
        if (currentLanguage == 0)
        {
            instruccion.text = "¿Cómo llegamos de " + places[randomOrigin].GetComponent<PlacePath>().objectNameA[0] + " a " + places[randomTarget].GetComponent<PlacePath>().objectNameA[0] + "?\n" + "Toca el lugar de inicio, los puntos por donde debes pasar y termina en el destino.";
            pronunciacion.text = "";
        }
        else if (currentLanguage == 1)
        {
            instruccion.text = "How do we get from " + places[randomOrigin].GetComponent<PlacePath>().objectNameA[1] + " to " + places[randomTarget].GetComponent<PlacePath>().objectNameA[1] + "?\n" + "Touch the start location, the points where you have to go through and then the destination.";
            pronunciacion.text = "";
        }
        else if (currentLanguage == 2)
        {
            instruccion.text = "Comment nous arrivons de " + places[randomOrigin].GetComponent<PlacePath>().objectNameA[2] + " à " + places[randomTarget].GetComponent<PlacePath>().objectNameA[2] + "?\n" + "Touchez le lieu de départ, les points par lesquels tu dois passer, puis la destination.";
            pronunciacion.text = "";
        }
        else if (currentLanguage == 3)//german
        {
            instruccion.text = "Wie kommen wir von " + places[randomOrigin].GetComponent<PlacePath>().objectNameA[3] + " nach " + places[randomTarget].GetComponent<PlacePath>().objectNameA[3] + "?\n" + "Berühren Sie den Startort, die Punkte, durch die Sie gehen müssen, und dann das Ziel.";
            pronunciacion.text = "";
        }
        else if (currentLanguage == 4)//italian
        {
            instruccion.text = "Come si arriva da " + places[randomOrigin].GetComponent<PlacePath>().objectNameA[4] + " a " + places[randomTarget].GetComponent<PlacePath>().objectNameA[4] + "?\n" + "Tocca la posizione di partenza, i punti da attraversare e poi la destinazione.";
            pronunciacion.text = "";
        }
        else if (currentLanguage == 5)//portuguese
        {
            instruccion.text = "Como vamos de " + places[randomOrigin].GetComponent<PlacePath>().objectNameA[5] + " para " + places[randomTarget].GetComponent<PlacePath>().objectNameA[5] + "?\n" + "Toque no local de partida, nos pontos por onde deve passar e depois no destino.";
            pronunciacion.text = "";
        }
        else if (currentLanguage == 6)//chinsese
        {
            instruccion.text = "我们如何从" + places[randomOrigin].GetComponent<PlacePath>().objectNameA[6] + "到" + places[randomTarget].GetComponent<PlacePath>().objectNameA[6] + "?\n" + "触摸开始位置，必须经过的地点，然后触摸目的地。";
            pronunciacion.text = "Wǒmen rúhé cóng " + places[randomOrigin].GetComponent<PlacePath>().objectNameA[9] + " dào " + places[randomTarget].GetComponent<PlacePath>().objectNameA[9] + "?\n" + "Chùmō kāishǐ wèizhì, bìxū jīngguò dì dìdiǎn, ránhòu chùmō mùdì de.";
        }
        else if (currentLanguage == 7)//korean
        {
            instruccion.text = places[randomOrigin].GetComponent<PlacePath>().objectNameA[7] + "에서 " + places[randomTarget].GetComponent<PlacePath>().objectNameA[7] + "로어떻게 가나 요?\n" + "시작 위치, 통과해야하는 지점, 목적지를 차례로 터치합니다.";
            pronunciacion.text = places[randomOrigin].GetComponent<PlacePath>().objectNameA[10] + " eseo " + places[randomTarget].GetComponent<PlacePath>().objectNameA[10] + "lo eotteohge gana yo?\n" + "sijag wichi, tong-gwahaeyahaneun jijeom, mogjeogjileul chalyelo teochihabnida.";
        }
        else if (currentLanguage == 8)//arabic
        {
            instruccion.text = "كيف ننتقل من  " + places[randomOrigin].GetComponent<PlacePath>().objectNameA[8] + "   إلى  " + places[randomTarget].GetComponent<PlacePath>().objectNameA[8] + "؟\n" + "المس موقع البدء ، والنقاط التي يتعين عليك المرور من خلالها ثم الوجهة.";
            pronunciacion.text = "kayf nantaqil min " + places[randomOrigin].GetComponent<PlacePath>().objectNameA[11] + " 'iilaa " + places[randomTarget].GetComponent<PlacePath>().objectNameA[11] + "?\n" + "'almusa mawqie albad' , walniqat alty yataeayan ealayk almurur min khilaliha thuma alwijhat.";
        }
    }

    public void Buttons (string type)
    {
        switch (type)
        {
            case "play":
                rollDice = true;
                //escojo un punto a y un punto b
                randomOrigin = Random.Range(0, places.Length);
                origin1 = randomOrigin;
                do
                {
                    randomTarget = Random.Range(0, places.Length);
                }
                while (randomOrigin == randomTarget);
                target1 = randomTarget;
                LanguageShowCase();
                playBtn.SetActive(false);
                break;
            case "library":
                id = 0;
                buttonPressed = true;
                break;
            case "shop":
                buttonPressed = true;
                id = 1;
                break;
            case "school":
                buttonPressed = true;
                id = 2;
                break;
            case "house":
                buttonPressed = true;
                id = 3;
                break;
            case "hospital":
                buttonPressed = true;
                id = 4;
                break;
            case "restaurant":
                buttonPressed = true;
                id = 5;
                break;
            case "gym":
                buttonPressed = true;
                id = 6;
                break;
            case "A":
                buttonPressed = true;
                id = 7;
                break;
            case "B":
                buttonPressed = true;
                id = 8;
                break;
            case "C":
                buttonPressed = true;
                id = 9;
                break;
            case "D":
                buttonPressed = true;
                id = 10;
                break;
            case "E":
                buttonPressed = true;
                id = 11;
                break;
            case "win":
                //apagar todo
                check1.SetActive(false); check2.SetActive(false); check3.SetActive(false); check4.SetActive(false); check5.SetActive(false); check6.SetActive(false);
                rounds++;
                rollDice = true;
                do
                {
                    randomOrigin = Random.Range(0, places.Length);
                }
                while (randomOrigin == origin1);
                origin1 = randomOrigin;
                do
                {
                    randomTarget = Random.Range(0, places.Length);
                }
                while (randomOrigin == randomTarget && randomTarget == target1);
                LanguageShowCase();
                if (rounds >= 3)
                {
                    gameover.SetActive(true);
                }
                win.SetActive(false);
                break;
            case "close":
                container.SetActive(false);
                break;
            case "spanish":
                currentLanguage = 0;
                LanguageShowCase();
                break;
            case "english":
                currentLanguage = 1;
                LanguageShowCase();
                break;
            case "french":
                currentLanguage = 2;
                LanguageShowCase();
                break;
            case "german":
                currentLanguage = 3;
                LanguageShowCase();
                break;
            case "italian":
                currentLanguage = 4;
                LanguageShowCase();
                break;
            case "portuguese":
                currentLanguage = 5;
                LanguageShowCase();
                break;
            case "chinese":
                currentLanguage = 6;
                LanguageShowCase();
                break;
            case "korean":
                currentLanguage = 7;
                LanguageShowCase();
                break;
            case "arabic":
                currentLanguage = 8;
                LanguageShowCase();
                break;
        }
    }
}
