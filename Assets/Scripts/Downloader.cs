﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

public class Downloader : MonoBehaviour {

    public GameObject canvas;
    public Text porcentaje, accion;
    public Image bar;
    private GameObject prefab;
    public Text debug;
    public int sceneID;
    float peso;

    void Awake() {
        accion.text = "Inicializando";
        StartCoroutine(LoadBundle());
    }

    IEnumerator LoadBundle() {
        string url = "https://www.mowisolutions.com/AssetBundles/and/mowibundle";
#if UNITY_IOS
      url = "https://www.mowisolutions.com/AssetBundles/ios/mowibundle";
#endif
        Debug.Log("starting");
        //Caching.ClearCache();
        Debug.Log("cache cleared");
        System.Net.WebRequest web = System.Net.WebRequest.Create(url);
        using (System.Net.WebResponse resp = web.GetResponse()) {
            float.TryParse(resp.ContentLength.ToString(), out peso);
        }
        peso /= 1048576;
        Debug.Log("peso: " + peso);
        using (UnityWebRequest uwr = UnityWebRequestAssetBundle.GetAssetBundle(url, (uint)6, 0))
        {
            uwr.SendWebRequest();
            accion.text = "Cargando archivos guardados, espera unos minutos";
            while (!uwr.isDone)
            {
                if (uwr.downloadProgress > 0)
                {
                    accion.text = "Descargando de servidor, espera unos minutos";
                    porcentaje.text = (int)(uwr.downloadProgress * 100) + "%";
                    bar.fillAmount = uwr.downloadProgress;
                }
                yield return new WaitForSeconds(0.1f);
            }
            if (uwr.isNetworkError || uwr.isHttpError)
            {
                debug.text = uwr.error;
                yield return new WaitForSeconds(5);
                SceneManager.LoadScene("menu");
            }
            else
            {
                AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(uwr);
                //prefab = bundle.LoadAsset<GameObject>("nombre.prefab");
                if (sceneID == 0)
                {
                    prefab = bundle.LoadAsset<GameObject>("AssetBundle.prefab");
                }
                else if (sceneID == 1)
                {
                    prefab = bundle.LoadAsset<GameObject>("RouletteBundle.prefab");
                }
                else if (sceneID == 2)
                {
                    prefab = bundle.LoadAsset<GameObject>("BooksBundle.prefab");
                    if (prefab == null)
                    {
                        Debug.Log("no hay prefab");
                    }
                    else
                    {
                        Debug.Log("sí hay sí hay");
                    }
                }
                Instantiate(prefab, Vector3.zero, Quaternion.identity);
                Destroy(canvas);
                Destroy(this.gameObject);
                //bundle.Unload(false);
            }
        }

            /*WWW bundleWWW = WWW.LoadFromCacheOrDownload(url, 1);
        Debug.Log("loaded");
        yield return bundleWWW;
        Debug.Log("yielded");
        while (!bundleWWW.isDone) {
            Debug.Log("in while");
            porcentaje.text = (int)(bundleWWW.progress*100) + "%";
            bar.fillAmount = bundleWWW.progress;
            yield return null;
        }
        Debug.Log("while passed");
        porcentaje.text = "100%";
        bar.fillAmount = 1;
        if (bundleWWW.error != null) {
            Debug.Log("errored");
            debug.text = bundleWWW.error;
            yield return new WaitForSeconds(5);
            SceneManager.LoadScene(0);
        } else {
            Debug.Log("not errored");
            AssetBundle assetBundle = bundleWWW.assetBundle;
            if (sceneID == 0)
            {
                prefab = assetBundle.LoadAsset<GameObject>("AssetBundle.prefab");
            }
            else if (sceneID == 1)
            {
                prefab = assetBundle.LoadAsset<GameObject>("RouletteBundle.prefab");
            }
            else if (sceneID == 2)
            {
                prefab = assetBundle.LoadAsset<GameObject>("BooksBundle.prefab");
                if (prefab == null)
                {
                    Debug.Log("no hay prefab");
                }
                else
                {
                    Debug.Log("sí hay sí hay");
                }
            }
            Instantiate(prefab, Vector3.zero, Quaternion.identity);
            Destroy(canvas);
            Destroy(this.gameObject);
        }*/
    }
}
