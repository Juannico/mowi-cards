﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class PageCode : DefaultObserverEventHandler
{
    public GameObject canvas;
    //TrackableBehaviour mTrackableBehaviour;
    public BookCode manager;
    [HideInInspector] public bool pageActive = false;
    [HideInInspector] public int state;//0 es que no ha sido rastreado, 1 es que ha sido rastreado, 2 es que aunque se rastree se cierra
    public int pageID;
    public GameObject[] pageObjects;
    // Start is called before the first frame update
    void Awake()
    {
        state = 0;//comenzamos en desactivado natural
        /*mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }*/
    }

    // Update is called once per frame
    void Update()
    {
        if (state == 1)//si estamos activados 
        {
            //Debug.Log("mycode state active");//activamos
            for (int i = 0; i<pageObjects.Length; i++)
            {
                pageObjects[i].SetActive(true);
            }
            pageActive = true;//esto debería activarse y lo hace pero no se refleja
            canvas.SetActive(true);//esto sí funciona
        }
        else if (state == 0 || state == 2)//si estamos desactivados natural o forzosamente
        {
            if (state == 0)
            {
                Debug.Log("mycode estado natural");
            }
            else
            {
                Debug.Log("mycode forzado");
            }
            for (int i = 0; i < pageObjects.Length; i++)
            {
                pageObjects[i].SetActive(false);
            }
            pageActive = false;
            manager.book1enabled = false;
            canvas.SetActive(false);//esto no está pasando
        }
    }
    /*public void OnTrackableStateChanged(
                                        TrackableBehaviour.Status previousStatus,
                                        TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            int tempid = pageID;
            if (state == 0)//si estamos desactivados natural, pasamos a activado
            {
                state = 1;//activado
            }
            else if (state == 2)//si estamos en desactivado forzado
            {
                if (tempid != pageID)//7y además no estamos en la misma página de la que venimos
                {
                    state = 1;//activamos; si no, no deberíamos hacer nada
                }
            }
            //pageActive = true;
            //manager.activateLanguage = true;
            //canvas.SetActive(true);
        }
        else
        {
            state = 0;//desactivado natural
            //pageActive = false;
            //Debug.Log("untracked");
            //manager.book1enabled = false;
            //manager.activateLanguage = false;
            //canvas.SetActive(false);
        }
    }*/

    public void TargetFound()
    {
        int tempid = pageID;
        if (state == 0)//si estamos desactivados natural, pasamos a activado
        {
            state = 1;//activado
        }
        else if (state == 2)//si estamos en desactivado forzado
        {
            if (tempid != pageID)//7y además no estamos en la misma página de la que venimos
            {
                state = 1;//activamos; si no, no deberíamos hacer nada
            }
        }
    }

    public void TargetLost()
    {
        state = 0;
    }
}
