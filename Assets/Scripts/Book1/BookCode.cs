﻿using System.Collections;
using System.Collections.Generic;
//using UnityEditor;
using UnityEngine;

public class BookCode : MonoBehaviour
{
    //[SerializeField] GameObject[] books;
    public AudioSource bookSound;
    //[SerializeField] AudioClip[] bookClips;
    //[SerializeField] string[] textosSpanish;
    public AudioClip[] audiosSpanish;
    //[SerializeField] string[] textosEnglish;
    public AudioClip[] audiosEnglish;
    //[SerializeField] string[] textosFrench;
    public AudioClip[] audiosFrench;
    //[SerializeField] string[] textosChinese;
    //public string[] textosPChinese;
    public AudioClip[] audiosChinese;
    //[SerializeField] string[] textosPortugese;
    public AudioClip[] audiosPortugese;
    //[SerializeField] string[] textosKorean;
    public AudioClip[] audiosKorean;
    //public string[] textosPKorean;
    //[SerializeField] string[] textosItalian;
    public AudioClip[] audiosItalian;
    //[SerializeField] string[] textosArabic;
    //public string[] textosPArabic;
    public AudioClip[] audiosArabic;
    //[SerializeField] string[] textosGerman;
    public AudioClip[] audiosGerman;
    public AudioClip abcSong, helloTeacherSong, myGardenSong, whatTimeIsIt, littleIndian, howareyou, clapyourhands, timefortea, onetwothree, happyday, takemyseat, thezoo, animalsinzoo, allbirds, weathersong, lovemountains;
    GameObject[] pages;
    public GameObject mainCanvas, game1, game2, game3;

    [SerializeField] GameObject button1, canvas1, languagePanel;
    int wordID = 0;
    int language = 0;
    [HideInInspector] public bool book1enabled = false;
    //[HideInInspector] public bool activateLanguage = false;
    //bool lockLanguageActivation = false;
    // Start is called before the first frame update
    void Start()
    {
        pages = GameObject.FindGameObjectsWithTag("Book");
        languagePanel.SetActive(false);
    }
    //Felipe
    public void generarPrefabs()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (pages.Length != 0)
        {
            for (int i = 0; i < pages.Length; i++)
            {
                if (pages[i].GetComponent<PageCode>().pageActive || pages[i].GetComponent<PageCode>().state == 1)
                {
                    languagePanel.SetActive(true);
                    return;//fin de método
                }
                languagePanel.SetActive(false);
            }
        }
    }

    void GenericButtonMethod()
    {
        if (!book1enabled)
        {
            bookSound.clip = audiosSpanish[wordID];
            book1enabled = true;
        }
        if (language == 0)
        {
            bookSound.clip = audiosSpanish[wordID];
        }
        else if (language == 1)
        {
            bookSound.clip = audiosEnglish[wordID];
        }
        else if (language == 2)
        {
            bookSound.clip = audiosFrench[wordID];
        }
        else if (language == 3)
        {
            bookSound.clip = audiosArabic[wordID];
        }
        else if (language == 4)
        {
            bookSound.clip = audiosChinese[wordID];
        }
        else if (language == 5)
        {
            bookSound.clip = audiosKorean[wordID];
        }
        else if (language == 6)
        {
            bookSound.clip = audiosPortugese[wordID];
        }
        else if (language == 7)
        {
            bookSound.clip = audiosItalian[wordID];
        }
        else if (language == 8)
        {
            bookSound.clip = audiosGerman[wordID];
        }
        bookSound.Play();
    }

    public void BookButton(int arrayNum)
    {
        wordID = arrayNum;
        GenericButtonMethod();
    }

    public void BookButtons (string type)
    {
        switch (type)
        {
            case "closeHUD":
                for (int i = 0; i < pages.Length; i++)
                {
                    if (pages[i].GetComponent<PageCode>().state == 1)
                    {
                        pages[i].GetComponent<PageCode>().state = 2;
                        languagePanel.SetActive(false);
                        return;//fin de método
                    }
                }
                break;
            //pg4
            case "playbtn_b1_p4":
                wordID = 0;
                bookSound.clip = audiosSpanish[wordID];
                bookSound.Play();
                button1.SetActive(false);
                break;
            case "spanish_b1_p4":
                bookSound.clip = audiosSpanish[wordID];
                bookSound.Play();
                break;
            case "english_b1_p4":
                bookSound.clip = audiosEnglish[wordID];
                bookSound.Play();
                break;
            case "french_b1_p4":
                bookSound.clip = audiosFrench[wordID];
                bookSound.Play();
                break;
            case "german_b1_p4":
                bookSound.clip = audiosGerman[wordID];
                bookSound.Play();
                break;
            case "italian_b1_p4":
                bookSound.clip = audiosItalian[wordID];
                bookSound.Play();
                break;
            case "portugese_b1_p4":
                bookSound.clip = audiosPortugese[wordID];
                bookSound.Play();
                break;
            //buttons
            case "spanish":
                language = 0;
                GenericButtonMethod();
                break;
            case "english":
                language = 1;
                GenericButtonMethod();
                break;
            case "french":
                language = 2;
                GenericButtonMethod();
                break;
            case "arabic":
                language = 3;
                GenericButtonMethod();
                break;
            case "chinese":
                language = 4;
                GenericButtonMethod();
                break;
            case "korean":
                language = 5;
                GenericButtonMethod();
                break;
            case "portugese":
                language = 6;
                GenericButtonMethod();
                break;
            case "italian":
                language = 7;
                GenericButtonMethod();
                break;
            case "german":
                language = 8;
                GenericButtonMethod();
                break;
                //pg5
            case "abtn_b1_p5":
                wordID = 1;
                GenericButtonMethod();
                break;
            case "bbtn_b1_p5":
                wordID = 2;
                GenericButtonMethod();
                break;
            case "cbtn_b1_p5":
                wordID = 3;
                GenericButtonMethod();
                break;
            case "dbtn_b1_p5":
                wordID = 4;
                GenericButtonMethod();
                break;
            case "ebtn_b1_p5":
                wordID = 5;
                GenericButtonMethod();
                break;
            case "fbtn_b1_p5":
                wordID = 6;
                GenericButtonMethod();
                break;
            case "gbtn_b1_p5":
                wordID = 7;
                GenericButtonMethod();
                break;
            case "hbtn_b1_p5":
                wordID = 8;
                GenericButtonMethod();
                break;
            //pg6
            case "ibtn_b1_p6":
                wordID = 9;
                GenericButtonMethod();
                break;
            case "jbtn_b1_p6":
                wordID = 10;
                GenericButtonMethod();
                break;
            case "kbtn_b1_p6":
                wordID = 11;
                GenericButtonMethod();
                break;
            case "lbtn_b1_p6":
                wordID = 12;
                GenericButtonMethod();
                break;
            case "mbtn_b1_p6":
                wordID = 13;
                GenericButtonMethod();
                break;
            case "nbtn_b1_p6":
                wordID = 14;
                GenericButtonMethod();
                break;
            case "obtn_b1_p6":
                wordID = 15;
                GenericButtonMethod();
                break;
            case "pbtn_b1_p6":
                wordID = 16;
                GenericButtonMethod();
                break;
            //pg7
            case "qbtn_b1_p7":
                wordID = 17;
                GenericButtonMethod();
                break;
            case "rbtn_b1_p7":
                wordID = 18;
                GenericButtonMethod();
                break;
            case "sbtn_b1_p7":
                wordID = 19;
                GenericButtonMethod();
                break;
            case "tbtn_b1_p7":
                wordID = 20;
                GenericButtonMethod();
                break;
            case "ubtn_b1_p7":
                wordID = 21;
                GenericButtonMethod();
                break;
            case "vbtn_b1_p7":
                wordID = 22;
                GenericButtonMethod();
                break;
            case "wbtn_b1_p7":
                wordID = 23;
                GenericButtonMethod();
                break;
            case "xbtn_b1_p7":
                wordID = 24;
                GenericButtonMethod();
                break;
            //pg 8
            case "ybtn_b1_p8":
                wordID = 25;
                GenericButtonMethod();
                break;
            case "zbtn_b1_p8":
                wordID = 26;
                GenericButtonMethod();
                break;
            case "abcbtn_b1_p8":
                bookSound.clip = abcSong;
                bookSound.Play();
                break;
            //pg11
            case "numbers_p11_b1":
                wordID = 27;
                GenericButtonMethod();
                break;
            case "dialogue1_p11_b1":
                wordID = 28;
                GenericButtonMethod();
                break;
            //pg12
            case "abcbtn_b1_p12":
                bookSound.clip = littleIndian;
                bookSound.Play();
                break;
            //pg13
            case "number1_b1_pg13":
                wordID = 67;
                GenericButtonMethod();
                break;
            case "number2_b1_pg13":
                wordID = 68;
                GenericButtonMethod();
                break;
            case "number3_b1_pg13":
                wordID = 69;
                GenericButtonMethod();
                break;
            case "number5_b1_pg13":
                wordID = 70;
                GenericButtonMethod();
                break;
            case "number6_b1_pg13":
                wordID = 71;
                GenericButtonMethod();
                break;
            case "number7_b1_pg13":
                wordID = 72;
                GenericButtonMethod();
                break;
            case "number9_b1_pg13":
                wordID = 73;
                GenericButtonMethod();
                break;
            case "number10_b1_pg13":
                wordID = 74;
                GenericButtonMethod();
                break;
            case "number12_b1_pg13":
                wordID = 75;
                GenericButtonMethod();
                break;
            //pg 15
            case "family1_b1_pg15":
                wordID = 76;
                GenericButtonMethod();
                break;
            case "family2_b1_pg15":
                wordID = 77;
                GenericButtonMethod();
                break;
            case "family3_b1_pg15":
                wordID = 78;
                GenericButtonMethod();
                break;
            case "family4_b1_pg15":
                wordID = 79;
                GenericButtonMethod();
                break;
            case "family5_b1_pg15":
                wordID = 80;
                GenericButtonMethod();
                break;
            case "family6_b1_pg15":
                wordID = 81;
                GenericButtonMethod();
                break;
            //pg 16
            case "abcbtn_b1_p16":
                bookSound.clip = howareyou;
                bookSound.Play();
                break;
            //pg17
            case "dialogue2_p17_b1":
                wordID = 29;
                GenericButtonMethod();
                break;
            case "dialogue3_p17_b1":
                wordID = 30;
                GenericButtonMethod();
                break;
            //pg 20
            case "bodypart1_p20_b1":
                wordID = 82;
                GenericButtonMethod();
                break;
            case "bodypart2_p20_b1":
                wordID = 83;
                GenericButtonMethod();
                break;
            case "bodypart3_p20_b1":
                wordID = 84;
                GenericButtonMethod();
                break;
            case "bodypart4_p20_b1":
                wordID = 85;
                GenericButtonMethod();
                break;
            case "bodypart5_p20_b1":
                wordID = 86;
                GenericButtonMethod();
                break;
            case "bodypart6_p20_b1":
                wordID = 87;
                GenericButtonMethod();
                break;
            case "bodypart7_p20_b1":
                wordID = 88;
                GenericButtonMethod();
                break;
            case "bodypart8_p20_b1":
                wordID = 89;
                GenericButtonMethod();
                break;
            case "bodypart9_p20_b1":
                wordID = 90;
                GenericButtonMethod();
                break;
            case "bodypart10_p20_b1":
                wordID = 91;
                GenericButtonMethod();
                break;
            case "bodypart11_p20_b1":
                wordID = 92;
                GenericButtonMethod();
                break;
            //pg 21
            case "look1_p21_b1":
                wordID = 93;
                GenericButtonMethod();
                break;
            case "look2_p21_b1":
                wordID = 94;
                GenericButtonMethod();
                break;
            case "look3_p21_b1":
                wordID = 95;
                GenericButtonMethod();
                break;
            case "look4_p21_b1":
                wordID = 96;
                GenericButtonMethod();
                break;
            case "look5_p21_b1":
                wordID = 97;
                GenericButtonMethod();
                break;
            //pg 22
            case "clapbtn_b1_p22":
                bookSound.clip = clapyourhands;
                bookSound.Play();
                break;
            //pg 24
            case "clothes1_b1_pg24":
                wordID = 98;
                GenericButtonMethod();
                break;
            case "clothes2_b1_pg24":
                wordID = 99;
                GenericButtonMethod();
                break;
            case "clothes3_b1_pg24":
                wordID = 100;
                GenericButtonMethod();
                break;
            case "clothes4_b1_pg24":
                wordID = 101;
                GenericButtonMethod();
                break;
            case "clothes5_b1_pg24":
                wordID = 102;
                GenericButtonMethod();
                break;
            case "clothes6_b1_pg24":
                wordID = 103;
                GenericButtonMethod();
                break;
            case "clothes7_b1_pg24":
                wordID = 104;
                GenericButtonMethod();
                break;
            case "clothes8_b1_pg24":
                wordID = 105;
                GenericButtonMethod();
                break;
            case "clothes9_b1_pg24":
                wordID = 106;
                GenericButtonMethod();
                break;
            //pg27
            case "house1_b1_pg27":
                wordID = 107;
                GenericButtonMethod();
                break;
            case "house2_b1_pg27":
                wordID = 108;
                GenericButtonMethod();
                break;
            case "house3_b1_pg27":
                wordID = 109;
                GenericButtonMethod();
                break;
            case "house4_b1_pg27":
                wordID = 110;
                GenericButtonMethod();
                break;
            case "house5_b1_pg27":
                wordID = 111;
                GenericButtonMethod();
                break;
            case "house6_b1_pg27":
                wordID = 112;
                GenericButtonMethod();
                break;
            //pg 30
            case "paint1_b1_pg30":
                wordID = 113;
                GenericButtonMethod();
                break;
            case "paint2_b1_pg30":
                wordID = 114;
                GenericButtonMethod();
                break;
            case "paint3_b1_pg30":
                wordID = 115;
                GenericButtonMethod();
                break;
            case "paint4_b1_pg30":
                wordID = 116;
                GenericButtonMethod();
                break;
            case "paint5_b1_pg30":
                wordID = 117;
                GenericButtonMethod();
                break;
            case "paint6_b1_pg30":
                wordID = 118;
                GenericButtonMethod();
                break;
            case "paint7_b1_pg30":
                wordID = 119;
                GenericButtonMethod();
                break;
            case "paint8_b1_pg30":
                wordID = 120;
                GenericButtonMethod();
                break;
            case "paint9_b1_pg30":
                wordID = 121;
                GenericButtonMethod();
                break;
            //pg 31
            case "color1_b1_pg31":
                wordID = 122;
                GenericButtonMethod();
                break;
            case "color2_b1_pg31":
                wordID = 123;
                GenericButtonMethod();
                break;
            case "color3_b1_pg31":
                wordID = 124;
                GenericButtonMethod();
                break;
            case "color4_b1_pg31":
                wordID = 125;
                GenericButtonMethod();
                break;
            case "color5_b1_pg31":
                wordID = 126;
                GenericButtonMethod();
                break;
            case "color6_b1_pg31":
                wordID = 127;
                GenericButtonMethod();
                break;
            //pg 32
            case "shape1_b1_pg31":
                wordID = 128;
                GenericButtonMethod();
                break;
            case "shape2_b1_pg31":
                wordID = 129;
                GenericButtonMethod();
                break;
            case "shape3_b1_pg31":
                wordID = 130;
                GenericButtonMethod();
                break;
            case "shape4_b1_pg31":
                wordID = 131;
                GenericButtonMethod();
                break;
            case "shape5_b1_pg31":
                wordID = 132;
                GenericButtonMethod();
                break;
            case "shape6_b1_pg31":
                wordID = 133;
                GenericButtonMethod();
                break;
            //pg32
            case "shape1_b1_pg33":
                wordID = 134;
                GenericButtonMethod();
                break;
            case "shape2_b1_pg33":
                wordID = 135;
                GenericButtonMethod();
                break;
            //pg35
            case "fruit1_b1_pg35":
                wordID = 136;
                GenericButtonMethod();
                break;
            case "fruit2_b1_pg35":
                wordID = 137;
                GenericButtonMethod();
                break;
            case "fruit3_b1_pg35":
                wordID = 138;
                GenericButtonMethod();
                break;
            case "fruit4_b1_pg35":
                wordID = 139;
                GenericButtonMethod();
                break;
            case "fruit5_b1_pg35":
                wordID = 140;
                GenericButtonMethod();
                break;
            case "fruit6_b1_pg35":
                wordID = 141;
                GenericButtonMethod();
                break;
            case "fruit7_b1_pg35":
                wordID = 142;
                GenericButtonMethod();
                break;
            case "fruit8_b1_pg35":
                wordID = 143;
                GenericButtonMethod();
                break;
            case "fruit9_b1_pg35":
                wordID = 144;
                GenericButtonMethod();
                break;
            case "fruit10_b1_pg35":
                wordID = 145;
                GenericButtonMethod();
                break;
            case "fruit11_b1_pg35":
                wordID = 146;
                GenericButtonMethod();
                break;
            case "fruit12_b1_pg35":
                wordID = 147;
                GenericButtonMethod();
                break;
            //pg 36
            case "fruit1_b1_pg36":
                wordID = 148;
                GenericButtonMethod();
                break;
            case "fruit2_b1_pg36":
                wordID = 149;
                GenericButtonMethod();
                break;
            //pg 38
            case "drinks1_b1_pg38":
                wordID = 150;
                GenericButtonMethod();
                break;
            case "drinks2_b1_pg38":
                wordID = 151;
                GenericButtonMethod();
                break;
            case "drinks3_b1_pg38":
                wordID = 152;
                GenericButtonMethod();
                break;
            case "drinks4_b1_pg38":
                wordID = 1533;
                GenericButtonMethod();
                break;
            case "drinks5_b1_pg38":
                wordID = 154;
                GenericButtonMethod();
                break;
            case "drinks6_b1_pg38":
                wordID = 155;
                GenericButtonMethod();
                break;
            //pg 39
            case "drinks1_b1_pg39":
                wordID = 156;
                GenericButtonMethod();
                break;
            case "drinks2_b1_pg39":
                wordID = 157;
                GenericButtonMethod();
                break;
            case "drinks3_b1_pg39":
                wordID = 158;
                GenericButtonMethod();
                break;
            case "drinks4_b1_pg39":
                wordID = 159;
                GenericButtonMethod();
                break;
            //pg 40
            case "teabtn_b1_p40":
                bookSound.clip = timefortea;
                bookSound.Play();
                break;
            //pg 42
            case "animals1_b1_p42":
                wordID = 160;
                GenericButtonMethod();
                break;
            case "animals2_b1_p42":
                wordID = 161;
                GenericButtonMethod();
                break;
            case "animals3_b1_p42":
                wordID = 162;
                GenericButtonMethod();
                break;
            case "animals4_b1_p42":
                wordID = 163;
                GenericButtonMethod();
                break;
            case "animals5_b1_p42":
                wordID = 164;
                GenericButtonMethod();
                break;
            case "animals6_b1_p42":
                wordID = 165;
                GenericButtonMethod();
                break;
            case "animals7_b1_p42":
                wordID = 166;
                GenericButtonMethod();
                break;
            case "animals8_b1_p42":
                wordID = 167;
                GenericButtonMethod();
                break;
            case "animals9_b1_p42":
                wordID = 168;
                GenericButtonMethod();
                break;
            //pg 40
            case "sheep1_b1_pg43":
                wordID = 169;
                GenericButtonMethod();
                break;
            case "sheep2_b1_pg43":
                wordID = 170;
                GenericButtonMethod();
                break;
            //pg 46
            case "space1_b1_pg46":
                wordID = 171;
                GenericButtonMethod();
                break;
            case "space2_b1_pg46":
                wordID = 172;
                GenericButtonMethod();
                break;
            case "space3_b1_pg46":
                wordID = 173;
                GenericButtonMethod();
                break;
            case "space4_b1_pg46":
                wordID = 174;
                GenericButtonMethod();
                break;
            case "space5_b1_pg46":
                wordID = 175;
                GenericButtonMethod();
                break;
            case "space6_b1_pg46":
                wordID = 176;
                GenericButtonMethod();
                break;
            case "space7_b1_pg46":
                wordID = 177;
                GenericButtonMethod();
                break;
            //pg25
            case "dialogue4_p25_b1":
                wordID = 31;
                GenericButtonMethod();
                break;
            case "shirt_p25_b1":
                wordID = 32;
                GenericButtonMethod();
                break;
            case "skirt_p25_b1":
                wordID = 33;
                GenericButtonMethod();
                break;
            //28
            case "dialogue5_p28_b1":
                wordID = 34;
                GenericButtonMethod();
                break;
            case "dialogue6_p28_b1":
                wordID = 35;
                GenericButtonMethod();
                break;
            //pg44
            case "game1_p44_b1":
                GameObject singleton = GameObject.Find("Game1");
                if (singleton == null)
                {
                    Instantiate(game1, mainCanvas.transform);
                }
                else
                {
                    singleton.SetActive(true);
                }
                break;
            //book 2
            //pg 5
            case "dialogue1_p5_b2":
                wordID = 36;
                GenericButtonMethod();
                break;
            case "dialogue2_p5_b2":
                wordID = 37;
                GenericButtonMethod();
                break;
                //pg 6
            case "song_p6_b2":
                bookSound.clip = helloTeacherSong;
                bookSound.Play();
                break;
            //pg 11
            case "game_p11_b2":
                GameObject singleton2 = GameObject.Find("Game2");
                if (singleton2 == null)
                {
                    Debug.Log("mycode instanciando");
                    Instantiate(game2, mainCanvas.transform);
                }
                else
                {
                    Debug.Log("mycode ya hay alguien");
                    singleton2.SetActive(true);
                }
                break;
            //pg13
            case "livingroom_p13_b2":
                wordID = 38;
                GenericButtonMethod();
                break;
            case "bedroom_p13_b2":
                wordID = 39;
                GenericButtonMethod();
                break;
            case "garden_p13_b2":
                wordID = 40;
                GenericButtonMethod();
                break;
            case "kitchen_p13_b2":
                wordID = 41;
                GenericButtonMethod();
                break;
            case "diningroom_p13_b2":
                wordID = 42;
                GenericButtonMethod();
                break;
            case "bathroom_p13_b2":
                wordID = 43;
                GenericButtonMethod();
                break;
            //pg17
            case "song2_p17_b2":
                bookSound.clip = myGardenSong;
                bookSound.Play(); 
                break;
            //pg20
            case "dialogue3_p20_b2":
                wordID = 44;
                GenericButtonMethod();
                break;
            //pg42 y 43
            case "leon_p42_b2":
                wordID = 45;
                GenericButtonMethod();
                break;
            case "mono_p42_b2":
                wordID = 46;
                GenericButtonMethod();
                break;
            case "ciervo_p42_b2":
                wordID = 47;
                GenericButtonMethod();
                break;
            case "tigre_p42_b2":
                wordID = 48;
                GenericButtonMethod();
                break;
            case "oso_p42_b2":
                wordID = 49;
                GenericButtonMethod();
                break;
            case "panda_p43_b2":
                wordID = 50;
                GenericButtonMethod();
                break;
            case "serpiente_p43_b2":
                wordID = 51;
                GenericButtonMethod();
                break;
            case "zorro_p43_b2":
                wordID = 52;
                GenericButtonMethod();
                break;
            case "camello_p43_b2":
                wordID = 53;
                GenericButtonMethod();
                break;
            case "elefante_p43_b2":
                wordID = 54;
                GenericButtonMethod();
                break;
            //pg 44 y 54
            case "dialogue4_p44_b2":
                wordID = 55;
                GenericButtonMethod();
                break;
            case "dialogue5_p54_b2":
                wordID = 56;
                GenericButtonMethod();
                break;
            //pg 1
            case "kindergarden1_b2_pg1":
                wordID = 178;
                GenericButtonMethod();
                break;
            case "kindergarden2_b2_pg1":
                wordID = 179;
                GenericButtonMethod();
                break;
            case "kindergarden3_b2_pg1":
                wordID = 180;
                GenericButtonMethod();
                break;
            case "kindergarden4_b2_pg1":
                wordID = 181;
                GenericButtonMethod();
                break;
            case "kindergarden5_b2_pg1":
                wordID = 182;
                GenericButtonMethod();
                break;
            case "kindergarden6_b2_pg1":
                wordID = 183;
                GenericButtonMethod();
                break;
            case "kindergarden7_b2_pg1":
                wordID = 184;
                GenericButtonMethod();
                break;
            case "kindergarden8_b2_pg1":
                wordID = 185;
                GenericButtonMethod();
                break;
            case "kindergarden9_b2_pg1":
                wordID = 186;
                GenericButtonMethod();
                break;
            //pg 6
            case "stationery1_b2_pg6":
                wordID = 187;
                GenericButtonMethod();
                break;
            case "stationery2_b2_pg6":
                wordID = 188;
                GenericButtonMethod();
                break;
            case "stationery3_b2_pg6":
                wordID = 189;
                GenericButtonMethod();
                break;
            case "stationery4_b2_pg6":
                wordID = 190;
                GenericButtonMethod();
                break;
            case "stationery5_b2_pg6":
                wordID = 191;
                GenericButtonMethod();
                break;
            case "stationery6_b2_pg6":
                wordID = 192;
                GenericButtonMethod();
                break;
            case "stationery7_b2_pg6":
                wordID = 193;
                GenericButtonMethod();
                break;
            //pg 9
            case "room1_b2_pg9":
                wordID = 194;
                GenericButtonMethod();
                break;
            case "room2_b2_pg9":
                wordID = 195;
                GenericButtonMethod();
                break;
            //pg 11
            case "room1_b2_pg11":
                wordID = 196;
                GenericButtonMethod();
                break;
            case "room2_b2_pg11":
                wordID = 197;
                GenericButtonMethod();
                break;
            case "room3_b2_pg11":
                wordID = 198;
                GenericButtonMethod();
                break;
            case "room4_b2_pg11":
                wordID = 199;
                GenericButtonMethod();
                break;
            //pg 13
            case "DailyNeeds1_b2_pg13":
                wordID = 200;
                GenericButtonMethod();
                break;
            case "DailyNeeds2_b2_pg13":
                wordID = 201;
                GenericButtonMethod();
                break;
            case "DailyNeeds3_b2_pg13":
                wordID = 202;
                GenericButtonMethod();
                break;
            case "DailyNeeds4_b2_pg13":
                wordID = 203;
                GenericButtonMethod();
                break;
            case "DailyNeeds5_b2_pg13":
                wordID = 204;
                GenericButtonMethod();
                break;
            case "DailyNeeds6_b2_pg13":
                wordID = 205;
                GenericButtonMethod();
                break;
            case "DailyNeeds7_b2_pg13":
                wordID = 206;
                GenericButtonMethod();
                break;
            case "DailyNeeds8_b2_pg13":
                wordID = 207;
                GenericButtonMethod();
                break;
            //pg 15
            case "DailyNeeds1_b2_pg15":
                wordID = 208;
                GenericButtonMethod();
                break;
            case "DailyNeeds2_b2_pg15":
                wordID = 209;
                GenericButtonMethod();
                break;
                //pg 16
            case "DailyNeeds1_b2_pg16":
                wordID = 210;
                GenericButtonMethod();
                break;
            case "DailyNeeds2_b2_pg16":
                wordID = 211;
                GenericButtonMethod();
                break;
            case "DailyNeeds3_b2_pg16":
                wordID = 212;
                GenericButtonMethod();
                break;
            case "DailyNeeds4_b2_pg16":
                wordID = 213;
                GenericButtonMethod();
                break;
            case "DailyNeeds5_b2_pg16":
                wordID = 214;
                GenericButtonMethod();
                break;
            //pg 17
            case "BallGames1_b2_pg16":
                wordID = 215;
                GenericButtonMethod();
                break;
            case "BallGames2_b2_pg16":
                wordID = 216;
                GenericButtonMethod();
                break;
            case "BallGames3_b2_pg16":
                wordID = 217;
                GenericButtonMethod();
                break;
            case "BallGames4_b2_pg16":
                wordID = 218;
                GenericButtonMethod();
                break;
            case "BallGames5_b2_pg16":
                wordID = 219;
                GenericButtonMethod();
                break;
            case "BallGames6_b2_pg16":
                wordID = 220;
                GenericButtonMethod();
                break;
            case "BallGames7_b2_pg16":
                wordID = 221;
                GenericButtonMethod();
                break;
            case "BallGames8_b2_pg16":
                wordID = 222;
                GenericButtonMethod();
                break;
            //pg 18
            case "myroom1_b2_pg18":
                wordID = 223;
                GenericButtonMethod();
                break;
            case "myroom2_b2_pg18":
                wordID = 224;
                GenericButtonMethod();
                break;
            case "myroom3_b2_pg18":
                wordID = 225;
                GenericButtonMethod();
                break;
            case "myroom4_b2_pg18":
                wordID = 226;
                GenericButtonMethod();
                break;
            //pg 19
            case "vegetables1_pg19_b2":
                wordID = 227;
                GenericButtonMethod();
                break;
            case "vegetables2_pg19_b2":
                wordID = 228;
                GenericButtonMethod();
                break;
            case "vegetables3_pg19_b2":
                wordID = 229;
                GenericButtonMethod();
                break;
            case "vegetables4_pg19_b2":
                wordID = 230;
                GenericButtonMethod();
                break;
            case "vegetables5_pg19_b2":
                wordID = 231;
                GenericButtonMethod();
                break;
            case "vegetables6_pg19_b2":
                wordID = 232;
                GenericButtonMethod();
                break;
            case "vegetables7_pg19_b2":
                wordID = 233;
                GenericButtonMethod();
                break;
            case "vegetables8_pg19_b2":
                wordID = 234;
                GenericButtonMethod();
                break;
            case "vegetables9_pg19_b2":
                wordID = 235;
                GenericButtonMethod();
                break;
            //pg 20
            case "vegetables1_b2_pg20":
                wordID = 236;
                GenericButtonMethod();
                break;
            case "vegetables2_b2_pg20":
                wordID = 237;
                GenericButtonMethod();
                break;
            case "vegetables3_b2_pg20":
                wordID = 238;
                GenericButtonMethod();
                break;
            //pg 21
            case "actions1_b2_pg21":
                wordID = 239;
                GenericButtonMethod();
                break;
            case "actions2_b2_pg21":
                wordID = 240;
                GenericButtonMethod();
                break;
            case "actions3_b2_pg21":
                wordID = 241;
                GenericButtonMethod();
                break;
            case "actions4_b2_pg21":
                wordID = 242;
                GenericButtonMethod();
                break;
            case "actions5_b2_pg21":
                wordID = 243;
                GenericButtonMethod();
                break;
            case "actions6_b2_pg21":
                wordID = 244;
                GenericButtonMethod();
                break;
            case "actions7_b2_pg21":
                wordID = 245;
                GenericButtonMethod();
                break;
            case "actions8_b2_pg21":
                wordID = 246;
                GenericButtonMethod();
                break;
            //pg 22
            case "actions1_b2_pg22":
                wordID = 247;
                GenericButtonMethod();
                break;
            case "actions2_b2_pg22":
                wordID = 248;
                GenericButtonMethod();
                break;
            case "actions3_b2_pg22":
                wordID = 249;
                GenericButtonMethod();
                break;
            case "actions4_b2_pg22":
                wordID = 250;
                GenericButtonMethod();
                break;
            case "actions5_b2_pg22":
                wordID = 251;
                GenericButtonMethod();
                break;
            case "actions6_b2_pg22":
                wordID = 252;
                GenericButtonMethod();
                break;
            case "actions7_b2_pg22":
                bookSound.clip = onetwothree;
                bookSound.Play();
                break;
            //pg 23
            case "taste1_b2_pg23":
                wordID = 253;
                GenericButtonMethod();
                break;
            case "taste2_b2_pg23":
                wordID = 254;
                GenericButtonMethod();
                break;
            case "taste3_b2_pg23":
                wordID = 255;
                GenericButtonMethod();
                break;
            case "taste4_b2_pg23":
                wordID = 256;
                GenericButtonMethod();
                break;
            case "taste5_b2_pg23":
                wordID = 257;
                GenericButtonMethod();
                break;
            //pg 24
            case "taste1_b2_pg24":
                wordID = 258;
                GenericButtonMethod();
                break;
            case "taste2_b2_pg24":
                wordID = 259;
                GenericButtonMethod();
                break;
            case "taste3_b2_pg24":
                wordID = 260;
                GenericButtonMethod();
                break;
            case "taste4_b2_pg24":
                wordID = 261;
                GenericButtonMethod();
                break;
            case "taste5_b2_pg24":
                wordID = 262;
                GenericButtonMethod();
                break;
            case "taste6_b2_pg24":
                wordID = 263;
                GenericButtonMethod();
                break;
            case "taste7_b2_pg24":
                wordID = 264;
                GenericButtonMethod();
                break;
            case "taste8_b2_pg24":
                wordID = 265;
                GenericButtonMethod();
                break;
            //pg 25
            case "taste_b2_pg25":
                bookSound.clip = happyday;
                bookSound.Play();
                break;
            //pg 26
            case "transport1_b2_pg26":
                wordID = 266;
                GenericButtonMethod();
                break;
            case "transport2_b2_pg26":
                wordID = 267;
                GenericButtonMethod();
                break;
            case "transport3_b2_pg26":
                wordID = 268;
                GenericButtonMethod();
                break;
            case "transport4_b2_pg26":
                wordID = 269;
                GenericButtonMethod();
                break;
            case "transport5_b2_pg26":
                wordID = 270;
                GenericButtonMethod();
                break;
            case "transport6_b2_pg26":
                wordID = 271;
                GenericButtonMethod();
                break;
            case "transport7_b2_pg26":
                wordID = 272;
                GenericButtonMethod();
                break;
            case "transport8_b2_pg26":
                wordID = 273;
                GenericButtonMethod();
                break;
            case "transport9_b2_pg26":
                wordID = 274;
                GenericButtonMethod();
                break;
            //pg 27
            case "transport1_b2_pg27":
                wordID = 275;
                GenericButtonMethod();
                break;
            case "transport2_b2_pg27":
                wordID = 276;
                GenericButtonMethod();
                break;
            case "transport3_b2_pg27":
                wordID = 277;
                GenericButtonMethod();
                break;
            //pg 28
            case "transport1_b2_pg28":
                wordID = 278;
                GenericButtonMethod();
                break;
            case "transport2_b2_pg28":
                wordID = 279;
                GenericButtonMethod();
                break;
            case "transport3_b2_pg28":
                wordID = 280;
                GenericButtonMethod();
                break;
            case "transport4_b2_pg28":
                wordID = 281;
                GenericButtonMethod();
                break;
            case "transport5_b2_pg28":
                wordID = 282;
                GenericButtonMethod();
                break;
            case "transport6_b2_pg28":
                wordID = 283;
                GenericButtonMethod();
                break;
            //pg 29
            case "transport_b2_pg29":
                bookSound.clip = takemyseat;
                bookSound.Play();
                break;
            //pg 33
            case "zoo_b2_pg33":
                bookSound.clip = thezoo;
                bookSound.Play();
                break;
            //pg 34
            case "zoo_b2_pg34":
                bookSound.clip = animalsinzoo;
                bookSound.Play();
                break;
            //pg 35 
            case "seasons1_b2_pg35":
                wordID = 284;
                GenericButtonMethod();
                break;
            case "seasons2_b2_pg35":
                wordID = 285;
                GenericButtonMethod();
                break;
            case "seasons3_b2_pg35":
                wordID = 286;
                GenericButtonMethod();
                break;
            case "seasons4_b2_pg35":
                wordID = 287;
                GenericButtonMethod();
                break;
            case "seasons5_b2_pg35":
                wordID = 288;
                GenericButtonMethod();
                break;
            case "seasons6_b2_pg35":
                wordID = 289;
                GenericButtonMethod();
                break;
            //pg 36
            case "seasons1_b2_pg36":
                wordID = 290;
                GenericButtonMethod();
                break;
            case "seasons2_b2_pg36":
                wordID = 291;
                GenericButtonMethod();
                break;
            case "seasons3_b2_pg36":
                wordID = 292;
                GenericButtonMethod();
                break;
            case "seasons4_b2_pg36":
                wordID = 293;
                GenericButtonMethod();
                break;
            case "seasons5_b2_pg36":
                wordID = 294;
                GenericButtonMethod();
                break;
            case "seasons6_b2_pg36":
                wordID = 295;
                GenericButtonMethod();
                break;
            case "seasons7_b2_pg36":
                wordID = 296;
                GenericButtonMethod();
                break;
            case "seasons8_b2_pg36":
                wordID = 297;
                GenericButtonMethod();
                break;
            case "seasons9_b2_pg36":
                wordID = 298;
                GenericButtonMethod();
                break;
            case "seasons10_b2_pg36":
                wordID = 299;
                GenericButtonMethod();
                break;
            case "seasons11_b2_pg36":
                wordID = 300;
                GenericButtonMethod();
                break;
            case "seasons12_b2_pg36":
                wordID = 301;
                GenericButtonMethod();
                break;
            //pg 37
            case "seasons_b2_pg37":
                bookSound.clip = allbirds;
                bookSound.Play();
                break;
            //pg 38
            case "weather1_b2_pg38":
                wordID = 302;
                GenericButtonMethod();
                break;
            case "weather2_b2_pg38":
                wordID = 303;
                GenericButtonMethod();
                break;
            case "weather3_b2_pg38":
                wordID = 304;
                GenericButtonMethod();
                break;
            case "weather4_b2_pg38":
                wordID = 305;
                GenericButtonMethod();
                break;
            case "weather5_b2_pg38":
                wordID = 306;
                GenericButtonMethod();
                break;
            //pg 39
            case "weather1_b2_pg39":
                wordID = 307;
                GenericButtonMethod();
                break;
            case "weather2_b2_pg39":
                wordID = 308;
                GenericButtonMethod();
                break;
            case "weather3_b2_pg39":
                wordID = 309;
                GenericButtonMethod();
                break;
            case "weather4_b2_pg39":
                wordID = 310;
                GenericButtonMethod();
                break;
            case "weather5_b2_pg39":
                wordID = 311;
                GenericButtonMethod();
                break;
            //pg 41
            case "weather_b2_pg41":
                bookSound.clip = weathersong;
                bookSound.Play();
                break;
            //pg 42
            case "nature1_b2_pg42":
                wordID = 312;
                GenericButtonMethod();
                break;
            case "nature2_b2_pg42":
                wordID = 313;
                GenericButtonMethod();
                break;
            case "nature3_b2_pg42":
                wordID = 314;
                GenericButtonMethod();
                break;
            case "nature4_b2_pg42":
                wordID = 315;
                GenericButtonMethod();
                break;
            case "nature5_b2_pg42":
                wordID = 316;
                GenericButtonMethod();
                break;
            case "nature6_b2_pg42":
                wordID = 317;
                GenericButtonMethod();
                break;
            case "nature7_b2_pg42":
                wordID = 318;
                GenericButtonMethod();
                break;
            case "nature8_b2_pg42":
                wordID = 319;
                GenericButtonMethod();
                break;
            case "nature9_b2_pg42":
                wordID = 320;
                GenericButtonMethod();
                break;
            case "nature10_b2_pg42":
                wordID = 321;
                GenericButtonMethod();
                break;
            //pg 43
            case "nature1_b2_pg43":
                wordID = 322;
                GenericButtonMethod();
                break;
            case "nature2_b2_pg43":
                wordID = 323;
                GenericButtonMethod();
                break;
            case "nature3_b2_pg43":
                wordID = 324;
                GenericButtonMethod();
                break;
            case "nature4_b2_pg43":
                wordID = 325;
                GenericButtonMethod();
                break;
            case "nature5_b2_pg43":
                wordID = 326;
                GenericButtonMethod();
                break;
            case "nature6_b2_pg43":
                wordID = 327;
                GenericButtonMethod();
                break;
            //pg 44
            case "nature_b2_pg44":
                bookSound.clip = lovemountains;
                bookSound.Play();
                break;
            //book 3
            //pg 3
            case "song3_p3_b3":
                bookSound.clip = whatTimeIsIt;
                bookSound.Play();
                break;
            //pg 5
            case "dialogue1_p5_b3":
                wordID = 57;
                GenericButtonMethod();
                break;
            case "dialogue2_p8_b3":
                wordID = 58;
                GenericButtonMethod();
                break;
            case "dialogue3_p9_b3":
                wordID = 59;
                GenericButtonMethod();
                break;
            case "dialogue4_p9_b3":
                wordID = 60;
                GenericButtonMethod();
                break;
            case "dialogue5_p12_b3":
                wordID = 61;
                GenericButtonMethod();
                break;
            case "dialogue6_p25_b3":
                wordID = 62;
                GenericButtonMethod();
                break;
            case "song2_p25_b3":
                wordID = 63;
                GenericButtonMethod();
                break;
            case "dialogue7_p27_b3":
                wordID = 64;
                GenericButtonMethod();
                break;
            case "dialogue8_p34_b3":
                wordID = 65;
                GenericButtonMethod();
                break;
            case "dialogue9_p35_b3":
                wordID = 66;
                GenericButtonMethod();
                break;
            case "game3_p38_b3":
                GameObject singleton3 = GameObject.Find("Game3");
                if (singleton3 == null)
                {
                    Instantiate(game3, mainCanvas.transform);
                }
                else
                {
                    singleton3.SetActive(true);
                }
                break;
        }
    }
 }
