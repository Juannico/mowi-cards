﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game1Manager : MonoBehaviour
{
    [SerializeField] Text animalType, pronunctiationAnimal, title;
    [SerializeField] string[] textosSpanish;
    public AudioClip[] audiosSpanish;
    [SerializeField] string[] textosEnglish;
    public AudioClip[] audiosEnglish;
    [SerializeField] string[] textosFrench;
    public AudioClip[] audiosFrench;
    [SerializeField] string[] textosChinese;
    public string[] textosPChinese;
    public AudioClip[] audiosChinese;
    [SerializeField] string[] textosPortugese;
    public AudioClip[] audiosPortugese;
    [SerializeField] string[] textosKorean;
    public AudioClip[] audiosKorean;
    public string[] textosPKorean;
    [SerializeField] string[] textosItalian;
    public AudioClip[] audiosItalian;
    [SerializeField] string[] textosArabic;
    public string[] textosPArabic;
    public AudioClip[] audiosArabic;
    [SerializeField] string[] textosGerman;
    public AudioClip[] audiosGerman;
    public AudioSource animales, audios;
    public AudioClip[] animalClips;
    int randomAnimal;
    [SerializeField] GameObject win, lose, gameover, playbtn, container;
    [SerializeField] GameObject[] animalBtns;
    [SerializeField] int[] randomArray;
    [SerializeField] private Animator[] btns_banderas;
    [SerializeField] private Animator[] anin_animales;
    [SerializeField] private GameObject contenedor_banderas;
    private int idioma = 0;
    public TutorialSystem tutorial;
    public AudioSource celebracion_sonido;
    public ParticleSystem particulas;
    void Start()
    {
        celebracion_sonido = particulas.GetComponent<AudioSource>();
        for (int i = 0; i < 9; i++)
        {
            do
            {
                randomAnimal = Random.Range(0, 9);
            }
            while (randomArray[randomAnimal] != -1);
            randomArray[randomAnimal] = i;
        }
        randomAnimal = 0;
        win.SetActive(false);
        lose.SetActive(false);
        gameover.SetActive(false);
        playbtn.SetActive(true);
        //título primera pregunta, primer animal, primera pronunciación
        title.text = "";
        pronunctiationAnimal.text = "";
        animalType.text = "";
        Invoke("lanzarTuto", 0.5f);
    }
    public void ocultarAvisos()
    {
        win.SetActive(false);
        lose.SetActive(false);
        Buttons("win");
    }
    public void ocultarAvisos2()
    {
        win.SetActive(false);
        lose.SetActive(false);
    }
    public void lanzarAnimaciones()
    {
        Buttons("play");
        for (int i = 0; i < animalBtns.Length; i++)
        {
            animalBtns[i].GetComponent<Animator>().SetTrigger("resaltar");
        }
    }
    public void lanzarTuto()
    {
        List<string> lista_instrucciones = new List<string>();
        lista_instrucciones.Add("Hola! vamos a jugar mientras aprendemos los animales");
        lista_instrucciones.Add("Selecciona el animal");
        tutorial.actualizarListaInstrucciones(lista_instrucciones);
        tutorial.abrirVentana();
    }

    private void OnEnable()
    {
        idioma = 0;
        contenedor_banderas.SetActive(true);
        title.text = textosSpanish[9];
        pronunctiationAnimal.text = "";
        if (randomArray[randomAnimal] != -1)
        {
            audios.clip = audiosSpanish[randomArray[randomAnimal]];
            audios.Play();
            animalType.text = textosSpanish[randomArray[randomAnimal]];
        }
        abrirBandera(0);
        Invoke("lanzarTuto", 0.5f);
    }
    public void Buttons(string animalButton)
    {
        switch (animalButton)
        {
            case "caballo":
                animalBtns[0].GetComponent<Animator>().SetTrigger("resaltar");
                if (randomArray[randomAnimal] == 0)//si mi animal actual es 0, gano
                {
                    //randomAnimal = 0;
                    animales.clip = animalClips[randomArray[randomAnimal]];
                    animales.Play();
                    win.SetActive(true); particulas.Play(); celebracion_sonido.Play();
                    
                    Invoke("ocultarAvisos", 2);
                }
                else
                {
                    lose.SetActive(true);
                    Invoke("ocultarAvisos2", 2);
                }

                break;
            case "perro":
                animalBtns[1].GetComponent<Animator>().SetTrigger("resaltar");
                if (randomArray[randomAnimal] == 1)//si mi animal actual es 0, gano
                {
                    //randomAnimal = 1;
                    animales.clip = animalClips[randomArray[randomAnimal]];
                    animales.Play();
                    //randomAnimal = 0;
                    win.SetActive(true); particulas.Play(); celebracion_sonido.Play();
                    Invoke("ocultarAvisos", 2);
                }
                else
                {
                    lose.SetActive(true);
                    Invoke("ocultarAvisos2", 2);

                }

                break;
            case "pato":
                animalBtns[2].GetComponent<Animator>().SetTrigger("resaltar");
                if (randomArray[randomAnimal] == 2)//si mi animal actual es 0, gano
                {
                    //randomAnimal = 2;
                    animales.clip = animalClips[randomArray[randomAnimal]];
                    animales.Play();
                    win.SetActive(true); particulas.Play(); celebracion_sonido.Play();
                    Invoke("ocultarAvisos", 2);

                }
                else
                {
                    lose.SetActive(true);
                    Invoke("ocultarAvisos2", 2);

                }

                break;
            case "ganso":
                animalBtns[3].GetComponent<Animator>().SetTrigger("resaltar");
                if (randomArray[randomAnimal] == 3)//si mi animal actual es 0, gano
                {
                    //randomAnimal = 3;
                    animales.clip = animalClips[randomArray[randomAnimal]];
                    animales.Play();
                    win.SetActive(true); particulas.Play(); celebracion_sonido.Play();
                    Invoke("ocultarAvisos", 2);
                }

                else
                {
                    lose.SetActive(true);
                    Invoke("ocultarAvisos2", 2);

                }

                break;
            case "vaca":
                animalBtns[4].GetComponent<Animator>().SetTrigger("resaltar");
                if (randomArray[randomAnimal] == 4)//si mi animal actual es 0, gano
                {
                    //randomAnimal = 4;
                    animales.clip = animalClips[randomArray[randomAnimal]];
                    animales.Play();
                    win.SetActive(true); particulas.Play(); celebracion_sonido.Play();
                    Invoke("ocultarAvisos", 2);
                }
                else
                {
                    lose.SetActive(true);
                    Invoke("ocultarAvisos2", 2);

                }

                break;
            case "oveja":
                animalBtns[5].GetComponent<Animator>().SetTrigger("resaltar"); ;
                if (randomArray[randomAnimal] == 5)//si mi animal actual es 0, gano
                {
                    //randomAnimal = 5;
                    animales.clip = animalClips[randomArray[randomAnimal]];
                    animales.Play();
                    win.SetActive(true); particulas.Play(); celebracion_sonido.Play();
                    Invoke("ocultarAvisos", 2);
                }
                else
                {
                    lose.SetActive(true);
                    Invoke("ocultarAvisos2", 2);

                }

                break;
            case "gato":
                animalBtns[6].GetComponent<Animator>().SetTrigger("resaltar");
                if (randomArray[randomAnimal] == 6)//si mi animal actual es 0, gano
                {
                    //randomAnimal = 6;
                    animales.clip = animalClips[randomArray[randomAnimal]];
                    animales.Play();
                    win.SetActive(true); particulas.Play(); celebracion_sonido.Play();
                    Invoke("ocultarAvisos", 2);
                }
                else
                {
                    lose.SetActive(true);
                    Invoke("ocultarAvisos2", 2);

                }

                break;
            case "gallo":
                animalBtns[7].GetComponent<Animator>().SetTrigger("resaltar");
                if (randomArray[randomAnimal] == 7)//si mi animal actual es 0, gano
                {
                    //randomAnimal = 7;
                    animales.clip = animalClips[randomArray[randomAnimal]];
                    animales.Play();
                    win.SetActive(true); particulas.Play(); celebracion_sonido.Play();
                    Invoke("ocultarAvisos", 2);
                }
                else
                {
                    lose.SetActive(true);
                    Invoke("ocultarAvisos2", 2);

                }
                break;
            case "cabra":
                animalBtns[8].GetComponent<Animator>().SetTrigger("resaltar");
                if (randomArray[randomAnimal] == 8)//si mi animal actual es 0, gano
                {
                    //randomAnimal = 8;
                    animales.clip = animalClips[randomArray[randomAnimal]];
                    animales.Play();
                    win.SetActive(true); particulas.Play(); celebracion_sonido.Play();
                    Invoke("ocultarAvisos", 2);
                }
                else
                {
                    lose.SetActive(true);
                    Invoke("ocultarAvisos2", 2);

                }

                break;
            case "play":
                abrirBandera(0);
                title.text = textosSpanish[9];
                pronunctiationAnimal.text = "";
                animalType.text = textosSpanish[randomArray[randomAnimal]];
                audios.clip = audiosSpanish[randomArray[randomAnimal]];
                audios.Play();
                playbtn.SetActive(false);
                break;
            case "close":
                randomAnimal = 0;
                win.SetActive(false);
                lose.SetActive(false);
                gameover.SetActive(false);
                playbtn.SetActive(true);
                //título primera pregunta, primer animal, primera pronunciación
                title.text = "";
                pronunctiationAnimal.text = "";
                animalType.text = "";
                container.SetActive(false);
                break;
            case "spanish":
                abrirBandera(0);
                idioma = 0;
                title.text = textosSpanish[9];
                pronunctiationAnimal.text = "";
                audios.clip = audiosSpanish[randomArray[randomAnimal]];
                audios.Play();
                animalType.text = textosSpanish[randomArray[randomAnimal]];
                break;
            case "english":
                abrirBandera(1);
                idioma = 1;
                title.text = textosEnglish[9];
                pronunctiationAnimal.text = "";
                audios.clip = audiosEnglish[randomArray[randomAnimal]];
                audios.Play();
                animalType.text = textosEnglish[randomArray[randomAnimal]];
                break;
            case "french":
                abrirBandera(2);
                idioma = 2;
                title.text = textosFrench[9];
                pronunctiationAnimal.text = "";
                audios.clip = audiosFrench[randomArray[randomAnimal]];
                audios.Play();
                animalType.text = textosFrench[randomArray[randomAnimal]];
                break;
            case "chinese":
                abrirBandera(6);
                idioma = 6;
                title.text = textosChinese[9];
                pronunctiationAnimal.text = textosPChinese[randomArray[randomAnimal]];
                audios.clip = audiosChinese[randomArray[randomAnimal]];
                animalType.text = textosChinese[randomArray[randomAnimal]];
                audios.Play();
                break;
            case "korean":
                abrirBandera(7);
                idioma = 7;
                title.text = textosKorean[9];
                pronunctiationAnimal.text = textosPKorean[randomArray[randomAnimal]];
                audios.clip = audiosKorean[randomArray[randomAnimal]];
                animalType.text = textosKorean[randomArray[randomAnimal]];
                audios.Play();
                break;
            case "arabic":
                abrirBandera(8);
                idioma = 8;
                title.text = textosArabic[9];
                pronunctiationAnimal.text = textosPArabic[randomArray[randomAnimal]];
                audios.clip = audiosArabic[randomArray[randomAnimal]];
                animalType.text = textosArabic[randomArray[randomAnimal]];
                audios.Play();
                break;
            case "portugese":
                abrirBandera(5);
                idioma = 5;
                title.text = textosPortugese[9];
                pronunctiationAnimal.text = "";
                audios.clip = audiosPortugese[randomArray[randomAnimal]];
                animalType.text = textosPortugese[randomArray[randomAnimal]];
                audios.Play();
                break;
            case "italian":
                abrirBandera(4);
                idioma = 4;
                title.text = textosItalian[9];
                pronunctiationAnimal.text = "";
                audios.clip = audiosItalian[randomArray[randomAnimal]];
                animalType.text = textosItalian[randomArray[randomAnimal]];
                audios.Play();
                break;
            case "german":
                abrirBandera(3);
                idioma = 3;
                title.text = textosGerman[9];
                pronunctiationAnimal.text = "";
                audios.clip = audiosGerman[randomArray[randomAnimal]];
                animalType.text = textosGerman[randomArray[randomAnimal]];
                audios.Play();
                break;
            case "win":
                CancelInvoke("ocultarAvisos");
                pronunctiationAnimal.text = "";
                randomAnimal++;
                if (randomAnimal > 8)
                {
                    contenedor_banderas.SetActive(false);
                    win.SetActive(false);
                    animalType.text = "";
                    title.text = "";
                    gameover.SetActive(true);
                    playbtn.SetActive(false);
                    break;
                }
                switch (idioma)
                {
                    case 0:
                        Buttons("spanish");
                        break;
                    case 1:
                        Buttons("english");
                        break;
                    case 2:
                        Buttons("french");
                        break;
                    case 3:
                        Buttons("german");
                        break;
                    case 4:
                        Buttons("italian");
                        break;
                    case 5:
                        Buttons("portugese");
                        break;
                    case 6:
                        Buttons("chinese");
                        break;
                    case 7:
                        Buttons("korean");
                        break;
                    case 8:
                        Buttons("arabic");
                        break;
                }

                win.SetActive(false);
                break;
            case "lose":
                lose.SetActive(false);
                break;
        }
    }
    public void abrirBandera(int idiomaactual)
    {
        cerrarBanderas();
        btns_banderas[idiomaactual].SetBool("abrir", true);
        btns_banderas[idiomaactual].SetBool("cerrar", false);
    }
    private void cerrarBanderas()
    {
        for (int i = 0; i < btns_banderas.Length; i++)
        {
            btns_banderas[i].SetBool("cerrar", true);
            btns_banderas[i].SetBool("abrir", false);
        }
    }
}
