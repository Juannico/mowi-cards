using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdentificadorPresentacion : DefaultObserverEventHandler
{
    public bool yaBaje = false;
    public ControlDescargaAssets controldescarga;
    [HideInInspector] public GameObject miHijo = null;
    private void Awake()
    {
        base.OnTargetFound.AddListener(TargetFound);
        base.OnTargetLost.AddListener(TargetLost);
    }
    public void TargetFound()
    {
        if (!controldescarga.estoyocupado)
        {          
            if (!yaBaje)
            {             
                controldescarga.CargarAssetsBasic("presenbundle", gameObject.transform);
                controldescarga.target = gameObject;
            }
        }
    }
    public void TargetLost()
    {
        if (!controldescarga.estoyocupado && miHijo != null)
        {
            miHijo.gameObject.AddComponent<SeguidorARCam>();
            miHijo.gameObject.GetComponent<SeguidorARCam>().aSeguir = controldescarga.placeholder;
            miHijo.gameObject.GetComponent<SeguidorARCam>().seguir = true;
            controldescarga.placeholder.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
            controldescarga.placeholder.transform.localPosition = new Vector3(0, -70, 400);
            GetAllChildrenYActivarMesh(miHijo.gameObject.transform);
        }
    }

    public void GetAllChildrenYActivarMesh(Transform t)
    {
        List<GameObject> childGOs = new List<GameObject>();
        foreach (Transform childT in t)
        {
            childGOs.Add(childT.gameObject);
            if (childT.childCount > 0)
            {
                GetAllChildrenYActivarMesh(childT);
            }
        }
        foreach (GameObject prueba in childGOs)
        {
            SkinnedMeshRenderer ap;
            if (prueba.TryGetComponent<SkinnedMeshRenderer>(out ap))
            {
                ap.enabled = true;
            }
            MeshRenderer ap2;
            if (prueba.TryGetComponent<MeshRenderer>(out ap2))
            {
                ap2.enabled = true;
            }
        }
    }
}
