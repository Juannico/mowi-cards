
using UnityEngine;


/// <summary>
///  HUDManager
/// </summary>
[System.Serializable]
public class ClaseContenedoraTemp 
{
    public elementojsonIndividual[] audiosRepeticion;
    public elementojsonPalabra[] audios;
    
}
[System.Serializable]
public class elementojsonPalabra
{
    public string id;
    public string txtSpanish, txtEnglish, txtFrench, txtGerman, txtChinese, txtsPortugese, txtItalian, txtArabic, txtKorean, txtPChinese, txtPArabic, txtPKorean;
    public string audioSpanish, audioEnglish, audioFrench, audioGerman, audioChinese, audiosPortugese, audioItalian, audioArabic, audioKorean;
    public string interactionSfx, sfx;
}
[System.Serializable]
public class elementojsonIndividual
{
    public string id;
    public string txtref;
}
/// <summary>
///  DialogueManager
/// </summary>
[System.Serializable]
public class ClaseContenedoraDialogueTemp
{
    public elementojsonDiaolog[] dialogos;

}
[System.Serializable]
public class elementojsonDiaolog
{
    public string id;
    public string txtSpanish, txtEnglish, txtFrench, txtGerman, txtChinese, txtsPortugese, txtItalian, txtArabic, txtKorean, txtPChinese, txtPArabic, txtPKorean;
    public string audioSpanish, audioEnglish, audioFrench, audioGerman, audioChinese, audiosPortugese, audioItalian, audioArabic, audioKorean;
}

/// <summary>
///  Book Code
/// </summary>
[System.Serializable]
public class ClaseContenedoraBookTemp
{
    public elementojsonBook[] audios;
}
[System.Serializable]
public class elementojsonBook
{
    public string id;
    public string audioSpanish, audioEnglish, audioFrench, audioGerman, audioChinese, audiosPortugese, audioItalian, audioArabic, audioKorean;
}


/// <summary>
///  Ruleta
/// </summary>
[System.Serializable]
public class ClaseContenedoraRuletaTemp
{
    public elementojsonRuleta[] audios;
}
[System.Serializable]
public class elementojsonRuleta
{
    public string id;
    public string txtSpanish, txtEnglish, txtFrench, txtGerman, txtChinese, txtsPortugese, txtItalian, txtArabic, txtKorean, txtPChinese, txtPArabic, txtPKorean;
    public string audioSpanish, audioEnglish, audioFrench, audioGerman, audioChinese, audiosPortugese, audioItalian, audioArabic, audioKorean;
    public string interactionSfx, sfx;
}