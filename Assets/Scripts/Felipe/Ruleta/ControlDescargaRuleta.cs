using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlDescargaRuleta : MonoBehaviour
{
    public DownloaderFelipe downloader;
    [HideInInspector] public GameObject target;
    [HideInInspector] public bool estoyocupado = false;
    public GameObject placeholder, panelcarga;
    public TextAsset jsonruleta;
    public ControlRuleta controlruleta;
    public GameObject efecto;
    public void CargarAssets(string nombre)
    {
        panelcarga.SetActive(true);
        estoyocupado = true; 
        downloader.GetBundleObject(nombre, OnContentLoaded, target.transform);
    }
    void OnContentLoaded(GameObject assetp)
    {
        assetp.transform.localPosition = Vector3.zero;
        panelcarga.SetActive(false);
        estoyocupado = false;
        downloader.barracarga.fillAmount = 0;
        target.GetComponent<IdentificadorRuleta>().miHijo = assetp;
        target.GetComponent<IdentificadorRuleta>().yaBaje = true;
        controlruleta.reproducirAudio();
        GameObject a = Instantiate(efecto, assetp.transform);
        a.GetComponent<ParticleSystemRenderer>().enabled = true;
        a.transform.localScale = new Vector3(14, 14, 14);
    }
}
