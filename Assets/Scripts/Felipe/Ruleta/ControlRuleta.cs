using LitJson;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class ControlRuleta : MonoBehaviour
{
    [HideInInspector]public int idiomaactual = 0, palabraactual = -1;
    [HideInInspector]AudioSource[] audios = null;
    public Text txt_palabra2, txt_palabra;
    private bool isspin = false;
    public GameObject ruleta;
    public TextAsset jsonRuleta;
    private string jsonstring;
    private JsonData itemdata;
    [SerializeField] private Animator[] btns_banderas;
    public GameObject panelruleta, paneltarget;
    public GameObject obj_ganar, obj_perder;
    [HideInInspector] public GameObject target;
    public TutorialSystem tutorial;
    public Text txt_segundos;

    public int tiempo;
    private int segundos;
    public Animator anin_reloj;
    public GameObject obj_play, obj_lupa;

    public AudioSource celebracion_sonido;
    public ParticleSystem particulas;
    public void lanzarTuto()
    {
        List<string> lista_instrucciones = new List<string>();
        lista_instrucciones.Add("Hola! vamos a jugar mientras aprendemos");
        lista_instrucciones.Add("Gira la ruleta en el boton de Spin");
        lista_instrucciones.Add("Busca la tarjeta antes de que se acabe el tiempo");
        tutorial.actualizarListaInstrucciones(lista_instrucciones);
        tutorial.abrirVentana();
    }
    IEnumerator descontarTiempo()
    {
        obj_lupa.SetActive(true);
        yield return new WaitForSeconds(1);
        segundos--;
        txt_segundos.text = segundos.ToString();
        if (segundos == 5)
        {
            anin_reloj.ResetTrigger("parar");
            anin_reloj.SetTrigger("iniciar");
        }
        if (segundos > 0)
        {
            StartCoroutine("descontarTiempo");
        }
        else
        {
            anin_reloj.SetTrigger("parar");
            regresar();
        }
    }


    void Start()
    {
        celebracion_sonido = particulas.GetComponent<AudioSource>();
        abrirBandera();
        lanzarTuto();
        segundos = tiempo;
        txt_segundos.text = segundos.ToString();
        jsonstring = jsonRuleta.text;
        itemdata = JsonMapper.ToObject(jsonstring);
    }
    public void spinMetodo()
    {
        if (!isspin)
        {
            obj_play.SetActive(false);
            anin_reloj.SetTrigger("parar");
            segundos = tiempo;
            txt_segundos.text = segundos.ToString();
            StopAllCoroutines();
            StartCoroutine("Spin");
        }
          
    }
    public void buscar()
    {
        panelruleta.SetActive(false);
        paneltarget.SetActive(true);
    }
    public void regresar()
    {
        obj_lupa.SetActive(false);
        segundos = 0;
        txt_segundos.text = segundos.ToString();
        StopAllCoroutines();
        obj_play.SetActive(true);
        panelruleta.SetActive(true);
        paneltarget.SetActive(false);
        if(target != null)
        {
            target.GetComponent<IdentificadorRuleta>().OcultarObjeto();
        }
        palabraactual = -1;
        txt_palabra.text = "";
        txt_palabra2.text = "";
    }
    public void Menu()
    {
        SceneManager.LoadScene(0);
    }
    public void quitarElementos()
    {
        obj_ganar.SetActive(false);
        obj_perder.SetActive(false);
    }
    IEnumerator Spin()
    {       
        isspin = true;
        float totalAngle = 360 / 15;
        int random = Random.Range(200, 300);
        float timeInterval = 0.0001f * 2 * Time.deltaTime;
        for (int i = 0; i < random; i++)
        {
            ruleta.transform.Rotate(0, 0, totalAngle / 2);//inicio de rotaci�n
            //desaceleraci�n
            if (i > Mathf.RoundToInt(random * 0.2f))
            {
                timeInterval = 0.5f * Time.deltaTime;
            }
            if (i > Mathf.RoundToInt(random * 0.5f))
            {
                timeInterval = 1f * Time.deltaTime;
            }
            if (i > Mathf.RoundToInt(random * 0.7f))
            {
                timeInterval = 1.5f * Time.deltaTime;
            }
            if (i > Mathf.RoundToInt(random * 0.8f))
            {
                timeInterval = 2f * Time.deltaTime;
            }
            if (i > Mathf.RoundToInt(random * 0.9f))
            {
                timeInterval = 2.5f * Time.deltaTime;
            }
            yield return new WaitForSeconds(timeInterval);
        }
        if (Mathf.RoundToInt(ruleta.transform.eulerAngles.z) % totalAngle != 0)//por si cae entre dos n�meros
        {
            ruleta.transform.Rotate(0, 0, totalAngle / 2);
        }
        int finalAngle = Mathf.RoundToInt(ruleta.transform.eulerAngles.z);//redondeo del �ngulo
        for (int i = 0; i < 15; i++)
        {
            if (finalAngle == i * totalAngle)
            {
                palabraactual = i;
                colocarPalabra();
                txt_palabra2.text = "";
                segundos = tiempo;
                StartCoroutine("descontarTiempo");
            }
        }
        isspin = false;
    }
    public void colocarPalabra()
    {
        if (palabraactual != -1)
        {
            switch (idiomaactual)
            {
                case 0:
                    txt_palabra.text = itemdata["palabras"][palabraactual]["txtSpanish"].ToString();
                    break;
                case 1:
                    txt_palabra.text = itemdata["palabras"][palabraactual]["txtEnglish"].ToString();
                    break;
                case 2:
                    txt_palabra.text = itemdata["palabras"][palabraactual]["txtFrench"].ToString();
                    break;
                case 3:
                    txt_palabra.text = itemdata["palabras"][palabraactual]["txtGerman"].ToString();
                    break;
                case 4:
                    txt_palabra.text = itemdata["palabras"][palabraactual]["txtItalian"].ToString();
                    break;
                case 5:
                    txt_palabra.text = itemdata["palabras"][palabraactual]["txtsPortugese"].ToString();
                    break;
                case 6:
                    txt_palabra.text = itemdata["palabras"][palabraactual]["txtChinese"].ToString();
                    txt_palabra2.text = itemdata["palabras"][palabraactual]["txtPChinese"].ToString();
                    break;
                case 7:
                    txt_palabra.text = itemdata["palabras"][palabraactual]["txtKorean"].ToString();
                    txt_palabra2.text = itemdata["palabras"][palabraactual]["txtPKorean"].ToString();
                    break;
                case 8:
                    txt_palabra.text = itemdata["palabras"][palabraactual]["txtArabic"].ToString();
                    txt_palabra2.text = itemdata["palabras"][palabraactual]["txtPArabic"].ToString();
                    break;
            }
        }

    }
    public void reproducirAudio()
    {
        if (target!= null && palabraactual !=-1)
        {
            audios = target.transform.GetChild(0).GetComponents<AudioSource>();
            for (int i = 0; i < audios.Length; i++)
            {
                if (audios[i].isPlaying && i != 9)
                {
                    audios[i].Stop();
                }
            }
            audios[idiomaactual].Play();
        }       
    }
    public void cambiarIdioma(int id_idioma)
    {       
        idiomaactual = id_idioma;
        colocarPalabra();
        abrirBandera();
        if (palabraactual != -1)
        {
            reproducirAudio();
        }
    }
    public void abrirBandera()
    {
        cerrarBanderas();
        btns_banderas[idiomaactual].SetBool("abrir", true);
        btns_banderas[idiomaactual].SetBool("cerrar", false);
    }
    private void cerrarBanderas()
    {
        for (int i = 0; i < btns_banderas.Length; i++)
        {
            btns_banderas[i].SetBool("cerrar", true);
            btns_banderas[i].SetBool("abrir", false);
        }
    }
}
