using LitJson;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdentificadorRuleta : DefaultObserverEventHandler
{
    public int id;
    public ControlDescargaRuleta controldescarga;
    public ControlRuleta controlRuleta;
    [HideInInspector] public bool yaBaje = false;
    [HideInInspector] public GameObject miHijo = null;
    private string jsonstring;
    private JsonData itemdata;
    private void Awake()
    {
        base.OnTargetFound.AddListener(TargetFound);
        base.OnTargetLost.AddListener(TargetLost);
    }
    public void TargetFound()
    {
        if (!controlRuleta.panelruleta.activeSelf)
        {
            if (controlRuleta.palabraactual == id)
            {
                controlRuleta.obj_ganar.SetActive(true);
                controlRuleta.particulas.Play();
                controlRuleta.celebracion_sonido.Play();
     
                controlRuleta.Invoke("quitarElementos", 2);
                // Acert� 
                if (!controldescarga.estoyocupado)
                {
                    controldescarga.target = gameObject;
                    controlRuleta.target = gameObject;
                    if (yaBaje)
                    {
                        miHijo.gameObject.SetActive(true);
                        miHijo.gameObject.GetComponent<SeguidorARCam>().seguir = false;
                    }
                    else
                    {   
                        jsonstring = controlRuleta.jsonRuleta.text;
                        itemdata = JsonMapper.ToObject(jsonstring);
                        string nombre_ab = itemdata["palabras"][id]["txtEnglish"].ToString()
                .ToLower().Replace("_", string.Empty).Replace(" ", string.Empty) + "ruleta";
                        controldescarga.CargarAssets(nombre_ab + "bundle");
                    }
                }
            }
            else
            {
                controlRuleta.obj_perder.SetActive(true);
                controlRuleta.StopCoroutine("descontarTiempo");
                controlRuleta.Invoke("quitarElementos", 3);
            }
        }


    }
    public void TargetLost()
    {
        if (controlRuleta.palabraactual == id)
        {
            if (!controldescarga.estoyocupado && miHijo != null)
            {
                miHijo.gameObject.AddComponent<SeguidorARCam>();
                miHijo.gameObject.GetComponent<SeguidorARCam>().aSeguir = controldescarga.placeholder;
                miHijo.gameObject.GetComponent<SeguidorARCam>().seguir = true;
                GetAllChildrenYActivarMesh(miHijo.gameObject.transform);
            }
        }
    }
    public void OcultarObjeto()
    {
        DesactivarChildren();
    }
    public void DestroyAllChildren()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
    }
    public void DesactivarChildren()
    {
        miHijo.transform.localPosition = Vector3.zero;
        miHijo.transform.localRotation = Quaternion.identity;
        miHijo.gameObject.SetActive(false); 
    }
    public void GetAllChildrenYActivarMesh(Transform t)
    {
        List<GameObject> childGOs = new List<GameObject>();
        foreach (Transform childT in t)
        {
            childGOs.Add(childT.gameObject);
            if (childT.childCount > 0)
            {
                GetAllChildrenYActivarMesh(childT);
            }
        }
        foreach (GameObject prueba in childGOs)
        {
            SkinnedMeshRenderer ap;
            if (prueba.TryGetComponent<SkinnedMeshRenderer>(out ap))
            {
                ap.enabled = true;
            }
            MeshRenderer ap2;
            if (prueba.TryGetComponent<MeshRenderer>(out ap2))
            {
                ap2.enabled = true;
            }
            Canvas ap3;
            if (prueba.TryGetComponent<Canvas>(out ap3))
            {
                ap3.enabled = true;
            }
            SpriteRenderer ap4;
            if (prueba.TryGetComponent<SpriteRenderer>(out ap4))
            {
                ap4.enabled = true;
            }
        }
    }
}
