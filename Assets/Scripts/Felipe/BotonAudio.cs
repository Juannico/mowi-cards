using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BotonAudio : MonoBehaviour
{ 
    public IdentificadorTarjetaLibro mitarjeta;
    public bool yadescargue = false;
    public int id = 0;
    public void Start()
    {
        gameObject.GetComponent<Button>().onClick.AddListener(btnclic);
    }
    public void btnclic()
    {
        print(id);
        print(yadescargue);
        if (yadescargue)
        {
            mitarjeta.reproducirAudioDeBoton(id);
        }
        else
        {
            mitarjeta.descargarAudios(id, gameObject.name.Replace("in", string.Empty).Replace("en", string.Empty) + "bundle");
        }

    }
}
