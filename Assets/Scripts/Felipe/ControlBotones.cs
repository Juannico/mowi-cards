using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using System.IO;
using UnityEngine.UI;

public class ControlBotones : MonoBehaviour
{
    [HideInInspector] public GameObject objeto, objetodialogos = null;
    [HideInInspector] AudioSource[] audios = null, audios_dialogue = null;
    [HideInInspector] public AudioSource[] audioslibros = null;
    [SerializeField] private AudioClip[] audios_rep;
    public int id;
    public int iddialogos = 0;
    public int idiomaactual = 0;
    [SerializeField] private AudioSource audioSource_rep;
    public TextAsset jsonhud;
    private string jsonstring;
    private JsonData itemdata;
    public TextAsset jsondialogue;
    private string jsonstringdialogue;
    private JsonData itemdatadialogue;
    [SerializeField] private Text txt_palabra, txt_palabra2, txt_dialogos, txt_dialogos2;
    [SerializeField] private Animator[] btns_banderas;
    [SerializeField] private GameObject btn_dialogo;[SerializeField] private Image img_btndialogo;[SerializeField] private Text txt_btndialogo;[SerializeField] private Sprite spr_btndialogo;[SerializeField] private Sprite spr_btncerrar;
    [SerializeField] private ControlDescargaAssets controldescarga;
    public int modopalabras = 0;
    public Text txt_textodecarga;
    private void Start()
    {
        jsonstring = jsonhud.text;
        jsonstringdialogue = jsondialogue.text;
        itemdata = JsonMapper.ToObject(jsonstring);
        itemdatadialogue = JsonMapper.ToObject(jsonstringdialogue);
    }
    // id 0: espa�ol; id 1: ingles; id 2: frances; id 3: aleman;  id 4: italiano;  id 5: portugues; id 6: chino; id 7: coreano; id 8: arabe; 
    public void reproducirPalabra(int idb)
    {
        idiomaactual = idb;
        abrirBandera();
        if (modopalabras == 1)
        {
            reproducirDialogo();
        }
        else if (modopalabras == 0)
        {
            audios = objeto.GetComponents<AudioSource>();
            for (int i = 0; i < audios.Length; i++)
            {
                if (audios[i].isPlaying && i!=9)
                {
                    audios[i].Stop();
                }
            }
            audios[idb].Play();
            txt_palabra2.text = "";
            txt_dialogos2.text = ""; txt_dialogos.text = "";
            switch (idb)
            {
                case 0:
                    txt_palabra.text = itemdata["palabras"][id]["txtSpanish"].ToString();
                    break;
                case 1:
                    txt_palabra.text = itemdata["palabras"][id]["txtEnglish"].ToString();
                    break;
                case 2:
                    txt_palabra.text = itemdata["palabras"][id]["txtFrench"].ToString();
                    break;
                case 3:
                    txt_palabra.text = itemdata["palabras"][id]["txtGerman"].ToString();
                    break;
                case 4:
                    txt_palabra.text = itemdata["palabras"][id]["txtItalian"].ToString();
                    break;
                case 5:
                    txt_palabra.text = itemdata["palabras"][id]["txtsPortugese"].ToString();
                    break;
                case 6:
                    txt_palabra.text = itemdata["palabras"][id]["txtChinese"].ToString();
                    txt_palabra2.text = itemdata["palabras"][id]["txtPChinese"].ToString();
                    break;
                case 7:
                    txt_palabra.text = itemdata["palabras"][id]["txtKorean"].ToString();
                    txt_palabra2.text = itemdata["palabras"][id]["txtPKorean"].ToString();
                    break;
                case 8:
                    txt_palabra.text = itemdata["palabras"][id]["txtArabic"].ToString();
                    txt_palabra2.text = itemdata["palabras"][id]["txtPArabic"].ToString();
                    break;
            }
        }
        else if (modopalabras == 2)
        {
            abrirBandera();
            reproducirAudioLibros(idiomaactual);
        }
    }
    public void repetirDespues()
    {
        audioSource_rep.clip = audios_rep[idiomaactual];
        Invoke("auxiliarRepetir", audioSource_rep.clip.length);
        audioSource_rep.Play();
    }
    public void auxiliarRepetir()
    {
        reproducirPalabra(idiomaactual);
    }
    public void abrirBandera()
    {
        cerrarBanderas();
        btns_banderas[idiomaactual].SetBool("abrir", true);
        btns_banderas[idiomaactual].SetBool("cerrar", false);
    }
    private void cerrarBanderas()
    {
        for (int i = 0; i < btns_banderas.Length; i++)
        {
            btns_banderas[i].SetBool("cerrar", true);
            btns_banderas[i].SetBool("abrir", false);
        }
    }
    public void estadoBotonDialogo(bool estadop)
    {
        btn_dialogo.SetActive(estadop);
    }
    public void reproducirDialogo()
    {
        txt_palabra2.text = "";
        txt_dialogos2.text = "";
        txt_palabra.text = "";
        // bajar el bundle
        if (!objeto.transform.parent.GetComponent<IdentificadorTarjeta>().yabajedialogos && !controldescarga.estoyocupado)
        {
            string prueba = itemdata["palabras"][id]["txtEnglish"].ToString().ToLower() + "bundle" + "dialogue";
            prueba.Replace(" ", string.Empty);
            string nombre_ab = prueba.Replace(" ", string.Empty);
            controldescarga.CargarAssets(nombre_ab, id, iddialogos, 2);
            controldescarga.panelInstrucciones.SetActive(false);
            controldescarga.panelCarga.SetActive(true);
        }
        else
        {
            audios_dialogue = objetodialogos.GetComponents<AudioSource>();
            for (int i = 0; i < audios_dialogue.Length; i++)
            {
                if (audios_dialogue[i].isPlaying)
                {
                    audios_dialogue[i].Stop();
                }
            }
            audios_dialogue[idiomaactual].Play();
            switch (idiomaactual)
            {
                case 0:
                    txt_dialogos.text = itemdatadialogue["dialogos"][iddialogos]["txtSpanish"].ToString();
                    break;
                case 1:
                    txt_dialogos.text = itemdatadialogue["dialogos"][iddialogos]["txtEnglish"].ToString();
                    break;
                case 2:
                    txt_dialogos.text = itemdatadialogue["dialogos"][iddialogos]["txtFrench"].ToString();
                    break;
                case 3:
                    txt_dialogos.text = itemdatadialogue["dialogos"][iddialogos]["txtGerman"].ToString();
                    break;
                case 4:
                    txt_dialogos.text = itemdatadialogue["dialogos"][iddialogos]["txtItalian"].ToString();
                    break;
                case 5:
                    txt_dialogos.text = itemdatadialogue["dialogos"][iddialogos]["txtsPortugese"].ToString();
                    break;
                case 6:
                    txt_dialogos.text = itemdatadialogue["dialogos"][iddialogos]["txtChinese"].ToString();
                    txt_dialogos2.text = itemdatadialogue["dialogos"][iddialogos]["txtPChinese"].ToString();
                    break;
                case 7:
                    txt_dialogos.text = itemdatadialogue["dialogos"][iddialogos]["txtKorean"].ToString();
                    txt_dialogos2.text = itemdatadialogue["dialogos"][iddialogos]["txtPKorean"].ToString();
                    break;
                case 8:
                    txt_dialogos.text = itemdatadialogue["dialogos"][iddialogos]["txtArabic"].ToString();
                    txt_dialogos2.text = itemdatadialogue["dialogos"][iddialogos]["txtPArabic"].ToString();
                    break;
            }
        }
    }
    public void reproducirDialogoBoton()
    {
        if (modopalabras == 1)
        {
            img_btndialogo.sprite = spr_btndialogo;
            img_btndialogo.color = new Color(1, 1, 1, 0.8f);
            txt_btndialogo.text = "Di�logo";
            modopalabras = 0;
            reproducirPalabra(idiomaactual);
        }
        else
        {
            img_btndialogo.sprite = spr_btncerrar;
            img_btndialogo.color = new Color(1, 1, 1, 0.3f);
            txt_btndialogo.text = " Cerrar di�logo";
            modopalabras = 1;
            reproducirDialogo();
        }
    }
    public void reproducirAudioLibros(int id_idiomap)
    {
        idiomaactual = id_idiomap;
        for (int i = 0; i < audioslibros.Length; i++)
        {
            if (audioslibros[i].isPlaying)
            {
                audioslibros[i].Stop();
            }
        }
        audioslibros[idiomaactual].Play();
    }

    public void ajustarBanderas(List<bool> listaidiomasactivos)
    {
        for (int i = 0; i < listaidiomasactivos.Count; i++)
        {
            if (listaidiomasactivos[i])
            {
                btns_banderas[i].gameObject.SetActive(true);
            }
            else
            {
                btns_banderas[i].gameObject.SetActive(false);
            }
        }
    }
}
