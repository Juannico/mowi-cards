using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialSystem : MonoBehaviour
{
    private List<string> lista_instrucciones;
    public Animator animator_ventana;
    public GameObject btn_siguiente;
    private int numero_total_instrucciones;
    public Text txt_texto_instrucciones;
    private int numero_actual;
    public GameObject peliculadeproteccion;

    public int codigoevento = 0;

    // Ordenes 
    public Game1Manager game1;
    public Game2Script game2;
    public void actualizarListaInstrucciones(List<string> listap)
    {
        lista_instrucciones = listap;
        numero_total_instrucciones = listap.Count;
        numero_actual = 0;
        btn_siguiente.SetActive(true);
        txt_texto_instrucciones.text = listap[0];
        if (numero_actual + 1 >= numero_total_instrucciones)
        {
            btn_siguiente.SetActive(false);
        }
    }
    public void siguiente()
    {
        numero_actual++;
        txt_texto_instrucciones.text = lista_instrucciones[numero_actual];
        if(numero_actual+1 >= numero_total_instrucciones)
        {
            btn_siguiente.SetActive(false);
        }
    }
    public void cerrarVentana()
    {
 
        animator_ventana.SetTrigger("salida");
        Invoke("evento", 1f);
    }
    public void abrirVentana()
    {
        peliculadeproteccion.SetActive(true);
        animator_ventana.SetTrigger("entrada");
    }

    public void evento()
    {
        evento0();
        if (codigoevento == 1)
        {
            evento1();
        }
        else if (codigoevento == 2)
        {
            evento2();
        }
        else if (codigoevento == 3)
        {
            evento3();
        }
    }

    public void evento0()
    {
        peliculadeproteccion.SetActive(false);
    }
    public void evento1()
    {
        game1.lanzarAnimaciones();
    }
    public void evento2()
    {
        game2.Buttons("play");
    }
    public void evento3()
    {

    }
}
