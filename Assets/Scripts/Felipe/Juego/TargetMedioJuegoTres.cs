using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class TargetMedioJuegoTres : MonoBehaviour, IPointerEnterHandler
{
    public JuegoTres juegotres;
    public GameObject miestrella;
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (juegotres.estoyOprimiendo)
        {
            miestrella.SetActive(true);
        }
    }
}
