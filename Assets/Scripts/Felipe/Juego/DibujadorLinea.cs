using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DibujadorLinea : MonoBehaviour
{
    public GameObject prefablinea;
    Coroutine drawing;
    public Camera camara;
    public JuegoTres juego;
    GameObject lineObject;

    bool estoyoprimiendo = false;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && juego.estoyOprimiendo)
        {
            estoyoprimiendo = true;
            StartLine();
        }
        else if (Input.GetMouseButtonUp(0) && estoyoprimiendo)
        {
            FinishLine();
        }
    }

    void StartLine()
    {
        if (drawing != null)
        {
            StopCoroutine(drawing);
        }
        drawing = StartCoroutine(DrawLine());
    }

    void FinishLine()
    {
        estoyoprimiendo = false;
        Destroy(lineObject, 2);
        StopCoroutine(drawing);
    }

    IEnumerator DrawLine()
    {

        //GameObject prevLine = 

        lineObject = Instantiate(prefablinea as GameObject, new Vector3(0, 0, 0), Quaternion.identity);
        LineRenderer line = lineObject.GetComponent<LineRenderer>();
        line.positionCount = 0;

        while (true)
        {
            Vector3 position =camara.ScreenToWorldPoint(Input.mousePosition);
            position.z = 0;
            line.positionCount++;
            line.SetPosition(line.positionCount - 1, position);
            yield return null;
        }
    }
}
