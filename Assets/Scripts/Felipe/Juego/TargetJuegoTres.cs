using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TargetJuegoTres : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler
{
    public JuegoTres juegotres;
    public int id;
    public GameObject miestrella;
    public GameObject miobjeto;
    private Vector3 scale; 
    private void Start()
    {
        //scale = miobjeto.transform.localScale;
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        juegotres.clicLugar(id);
        //miobjeto.transform.localScale = scale * 1.4f;
    }
    public void hacerpequeño()
    {
        miobjeto.transform.localScale = scale;
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (juegotres.estoyOprimiendo)
        {
            miestrella.SetActive(true);
            juegotres.puntobprueba = id;
        }
    }
}
