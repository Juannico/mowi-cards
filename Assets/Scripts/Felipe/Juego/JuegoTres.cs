﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class JuegoTres : MonoBehaviour
{
    public Text txt_pregunta, txt_pronun;
    [SerializeField] private Animator[] btns_banderas;
    private int idioma_actual = 0;
    private int puntoa = -1;
    private int puntob = -1;
    private int puntoaprueba = -1;
    public int puntobprueba = -1;
    private int turno;
    public PlacePath[] lugares;
    public GameObject ganar, perder;
    public GameObject[] estrellas;

    public GameObject contenedor;

    public AudioSource celebracion_sonido;
    public ParticleSystem particulas_celebracion;

    public bool estoyOprimiendo = false;

    //public TargetJuegoTres[] edificios;

    public TutorialSystem tutorial;

    private List<string> listacombinaciones;
    private int rutaactual = 0;

    public void lanzarTuto()
    {
        List<string> lista_instrucciones = new List<string>();
        lista_instrucciones.Add("Hola! vamos a jugar mientras aprendemos");
        lista_instrucciones.Add("Traza el camino de un punto a otro");
        lista_instrucciones.Add("¡Cuidado! no te salgas de la carretera");
        tutorial.actualizarListaInstrucciones(lista_instrucciones);
        tutorial.abrirVentana();
    }
    void Start()
    {
        celebracion_sonido = particulas_celebracion.GetComponent<AudioSource>();
        listacombinaciones = new List<string>();
        listacombinaciones.Add("01");
        listacombinaciones.Add("02");
        listacombinaciones.Add("03");
        listacombinaciones.Add("04");
        listacombinaciones.Add("10");
        listacombinaciones.Add("12");
        listacombinaciones.Add("13");
        listacombinaciones.Add("14");
        listacombinaciones.Add("21");
        listacombinaciones.Add("20");
        listacombinaciones.Add("23");
        listacombinaciones.Add("24");
        listacombinaciones.Add("30");
        listacombinaciones.Add("32");
        listacombinaciones.Add("31");
        listacombinaciones.Add("34");
        listacombinaciones.Add("40");
        listacombinaciones.Add("42");
        listacombinaciones.Add("41");
        listacombinaciones.Add("43");
        for (int i = 0; i < listacombinaciones.Count; i++)
        {
            for (int j = 0; j <20; j++ )
            {
                int puntoa1 = Random.Range(0, listacombinaciones.Count);
                int puntob1 = Random.Range(0, listacombinaciones.Count);
                string temp = listacombinaciones[puntoa1];
                listacombinaciones[puntoa1] = listacombinaciones[puntob1];
                listacombinaciones[puntob1] = temp;
            }
        }
        reiniciar();
        lanzarTuto();
    }
    private void OnEnable()
    {
        reiniciar();
        lanzarTuto();
    }
    public void play()
    {
        ganar.SetActive(false);
        seleccionarRuta();
        colocarTextoRuta();
    }
    private void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Invoke("soltarClic", 0.25f);
        }
    }

    public void soltarClic()
    {
        if (estoyOprimiendo)
        {
            if (puntobprueba == puntob)
            {
                ganar.SetActive(true);
                particulas_celebracion.Play();
                celebracion_sonido.Play();
                StartCoroutine("perderdesactivar");
                Invoke("play",1);
                //for (int i = 0; i <edificios.Length; i++ )
                //{
                //    edificios[i].hacerpequeño();
                //}
            }
            else
            {
                perderMetodo();
                //for (int i = 0; i < edificios.Length; i++)
                //{
                //    edificios[i].hacerpequeño();
                //}
            }
            estoyOprimiendo = false;
        }
    }
    public void seleccionarRuta()
    {
        string temp = listacombinaciones[rutaactual];

        puntoa = int.Parse(temp[0].ToString());
        puntob = int.Parse(temp[1].ToString());
        print(puntoa + "   " + puntob);
        rutaactual++;
        if (rutaactual >= listacombinaciones.Count)
        {
            rutaactual = 0;
        }

        //puntoa = Random.Range(0, 5);
        //puntob = Random.Range(0, 5);
        //while (puntoa == puntob)
        //{
        //    puntob = Random.Range(0, 5);
        //}
    }
    public void colocarTextoRuta()
    {
        switch (idioma_actual)
        {
            case 0:
                txt_pregunta.text = "¿Cómo llegamos de " + lugares[puntoa].objectNameA[0] + " a " + lugares[puntob].objectNameA[0] + " ?\n" + "Toca el lugar de inicio y luego el lugar destino.";
                txt_pronun.text = "";
                break;
            case 1:
                txt_pregunta.text = "How do we get from " + lugares[puntoa].objectNameA[1] + " to " + lugares[puntob].objectNameA[1] + "?\n" + "Touch the start location, the points where you have to go through and then the destination.";
                txt_pronun.text = "";
                break;
            case 2:
                txt_pregunta.text = "Comment nous arrivons de " + lugares[puntoa].objectNameA[2] + " à " + lugares[puntob].objectNameA[2] + "?\n" + "Touchez le lieu de départ, les points par lesquels tu dois passer, puis la destination.";
                txt_pronun.text = "";
                break;
            case 3:
                txt_pregunta.text = "Wie kommen wir von " + lugares[puntoa].objectNameA[3] + " nach " + lugares[puntob].objectNameA[3] + "?\n" + "Berühren Sie den Startort, die Punkte, durch die Sie gehen müssen, und dann das Ziel.";
                txt_pronun.text = "";
                break;
            case 4:
                txt_pregunta.text = "Come si arriva da " + lugares[puntoa].objectNameA[4] + " a " + lugares[puntob].objectNameA[4] + "?\n" + "Tocca la posizione di partenza, i punti da attraversare e poi la destinazione.";
                txt_pronun.text = "";
                break;
            case 5:
                txt_pregunta.text = "Como vamos de " + lugares[puntoa].objectNameA[5] + " para " + lugares[puntob].objectNameA[5] + "?\n" + "Toque no local de partida, nos pontos por onde deve passar e depois no destino.";
                txt_pronun.text = "";
                break;
            case 6:
                txt_pregunta.text = "我们如何从" + lugares[puntoa].objectNameA[6] + "到" + lugares[puntob].objectNameA[6] + "?\n" + "触摸开始位置，必须经过的地点，然后触摸目的地。";
                txt_pronun.text = "Wǒmen rúhé cóng " + lugares[puntoa].objectNameA[9] + " dào " + lugares[puntob].objectNameA[9] + "?\n" + "Chùmō kāishǐ wèizhì, bìxū jīngguò dì dìdiǎn, ránhòu chùmō mùdì de.";
                break;
            case 7:
                txt_pregunta.text = lugares[puntoa].objectNameA[7] + "에서 " + lugares[puntob].objectNameA[7] + "로어떻게 가나 요?\n" + "로어떻게 가나 요?\n" + "시작 위치, 통과해야하는 지점, 목적지를 차례로 터치합니다.";
                txt_pronun.text = lugares[puntoa].objectNameA[10] + " eseo " + lugares[puntob].objectNameA[10] + "lo eotteohge gana yo?\n" + "sijag wichi, tong-gwahaeyahaneun jijeom, mogjeogjileul chalyelo teochihabnida.";
                break;
            case 8:
                txt_pregunta.text = "كيف ننتقل من  " + lugares[puntoa].objectNameA[8] + "   إلى  " + lugares[puntob].objectNameA[8] + "؟\n" + "المس موقع البدء ، والنقاط التي يتعين عليك المرور من خلالها ثم الوجهة.";
                txt_pronun.text = "kayf nantaqil min " + lugares[puntoa].objectNameA[11] + " 'iilaa " + lugares[puntob].objectNameA[11] + "?\n" + "'almusa mawqie albad' , walniqat alty yataeayan ealayk almurur min khilaliha thuma alwijhat.";
                break;
        }
    }
    public void clicLugar(int id)
    {
        if (id == puntoa)
        {   
            estrellas[puntoa].SetActive(true);
            estoyOprimiendo = true;
        }
        else
        {
            perderMetodo();
        }
    }
    public void perderMetodo()
    {
        estoyOprimiendo = false;
        perder.SetActive(true);
        StartCoroutine("perderdesactivar");
    }
    IEnumerator perderdesactivar()
    {
        yield return new WaitForSeconds(1);
        perder.SetActive(false);
        ganar.SetActive(false);
        for (int i = 0; i < estrellas.Length; i++)
        {
            estrellas[i].SetActive(false);
        }
    }



    /// <summary>
    /// ////////////////////////////////////////////////////////////////////////
    /// </summary>
    public void reiniciar()
    {
        abrirBandera(0);
        puntoaprueba = -1;
        puntobprueba = -1;
        idioma_actual = 0;
        play();
        //colocarTextoInicial();
    }
    public void cerrar()
    {
        reiniciar();
        contenedor.SetActive(false);
    }

    public void colocarTextoInicial()
    {
        // Modificar con la traduccion correcta para cada idioma según el español
        switch (idioma_actual)
        {
            case 0:
                txt_pregunta.text = "Toca el lugar de inicio y luego el lugar destino.";
                txt_pronun.text = "";
                break;
            case 1:
                txt_pregunta.text = "Touch the start location, the points where you have to go through and then the destination.";
                txt_pronun.text = "";
                break;
            case 2:
                txt_pregunta.text = "Touchez le lieu de départ, les points par lesquels tu dois passer, puis la destination.";
                txt_pronun.text = "";
                break;
            case 3:
                txt_pregunta.text = "Berühren Sie den Startort, die Punkte, durch die Sie gehen müssen, und dann das Ziel.";
                txt_pronun.text = "";
                break;
            case 4:
                txt_pregunta.text = "Tocca la posizione di partenza, i punti da attraversare e poi la destinazione.";
                txt_pronun.text = "";
                break;
            case 5:
                txt_pregunta.text = "Toque no local de partida, nos pontos por onde deve passar e depois no destino.";
                txt_pronun.text = "";
                break;
            case 6:
                txt_pregunta.text = "触摸开始位置，必须经过的地点，然后触摸目的地。";
                txt_pronun.text = "Chùmō kāishǐ wèizhì, bìxū jīngguò dì dìdiǎn, ránhòu chùmō mùdì de.";
                break;
            case 7:
                txt_pregunta.text = "로어떻게 가나 요?\n" + "시작 위치, 통과해야하는 지점, 목적지를 차례로 터치합니다.";
                txt_pronun.text = "sijag wichi, tong-gwahaeyahaneun jijeom, mogjeogjileul chalyelo teochihabnida.";
                break;
            case 8:
                txt_pregunta.text = "المس موقع البدء ، والنقاط التي يتعين عليك المرور من خلالها ثم الوجهة.";
                txt_pronun.text = "'almusa mawqie albad' , walniqat alty yataeayan ealayk almurur min khilaliha thuma alwijhat.";
                break;
        }
    }

    // Start is called before the first frame update



    public void abrirBandera(int idiomaactual)
    {
        cerrarBanderas();
        btns_banderas[idiomaactual].SetBool("abrir", true);
        btns_banderas[idiomaactual].SetBool("cerrar", false);
        idioma_actual = idiomaactual;
        if (puntoa == -1)
        {
            colocarTextoInicial();
        }
        else
        {
            colocarTextoRuta();
        }
    }
    private void cerrarBanderas()
    {
        for (int i = 0; i < btns_banderas.Length; i++)
        {
            btns_banderas[i].SetBool("cerrar", true);
            btns_banderas[i].SetBool("abrir", false);
        }
    }
}
