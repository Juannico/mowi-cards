using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class TargetMaloJuegoTres : MonoBehaviour, IPointerEnterHandler
{
    public JuegoTres juegotres;
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (juegotres.estoyOprimiendo)
        {
            juegotres.perderMetodo();
        }
    }
}
