using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;
using LitJson;
using System.IO;
using UnityEditor;
using System;

public class IdentificadorTarjetaLibro : DefaultObserverEventHandler
{
    /*
     Eventos Libros 
	    hacer audio 
           - 1,8,3 = icansayabc ***
           - 2,6,0 = hello teacher/libro2/ ***
           - 2,17,0 = There's Something / libro2 ***
           - 3,3,0 = What Time Is It Song for Kids
	    
        Juego 2,11,0 - 1,44,0 - 3,38,0
    */
    public ControlDescargaAssets controldescarga;
    [HideInInspector] public bool yaBaje = false;
    [HideInInspector] public GameObject miHijo = null;
    public bool espa�ol = true, ingles = true, frances = true, aleman = true, italiano = true, portugues = true, chino = true, coreano = true, arabe = true; 

    [HideInInspector] public int actualidboton = 0;
    [HideInInspector] public GameObject mijuego = null;
    private string nombrejuego = "";
    private void Awake()
    {
        base.OnTargetFound.AddListener(TargetFound);
        base.OnTargetLost.AddListener(TargetLost);
    }
    /*   
   private void Start()
    {
        /*
        // Auxiliar
        jsonstring = File.ReadAllText(Application.dataPath + "/Scripts/Felipe/JsonCreados/jsonHUD.json");
        itemdata = JsonMapper.ToObject(jsonstring);

        for (int i = 0; i < 10; i++)
        {
            gameObject.AddComponent<AudioSource>();
        }
        AudioSource[] audios = gameObject.GetComponents<AudioSource>();
        print(itemdata["palabras"][id]["audioSpanish"].ToString());

        audios[0].clip = Resources.Load<AudioClip>(itemdata["palabras"][id]["audioSpanish"].ToString());
        audios[1].clip = Resources.Load<AudioClip>(itemdata["palabras"][id]["audioEnglish"].ToString());
        audios[2].clip = Resources.Load<AudioClip>(itemdata["palabras"][id]["audioFrench"].ToString());
        audios[3].clip = Resources.Load<AudioClip>(itemdata["palabras"][id]["audioGerman"].ToString());
        audios[4].clip = Resources.Load<AudioClip>(itemdata["palabras"][id]["audioItalian"].ToString());
        audios[5].clip = Resources.Load<AudioClip>(itemdata["palabras"][id]["audiosPortugese"].ToString());
        audios[6].clip = Resources.Load<AudioClip>(itemdata["palabras"][id]["audioChinese"].ToString());
        audios[7].clip = Resources.Load<AudioClip>(itemdata["palabras"][id]["audioKorean"].ToString());
        audios[8].clip = Resources.Load<AudioClip>(itemdata["palabras"][id]["audioArabic"].ToString());
        audios[9].clip = Resources.Load<AudioClip>(itemdata["palabras"][id]["sfx"].ToString());
             
        string namebundle = gameObject.name.ToLower().Replace("_", string.Empty).Replace(" ", string.Empty);
        string assetPath = AssetDatabase.GetAssetPath(Selection.activeInstanceID);
        AssetImporter.GetAtPath(assetPath).SetAssetBundleNameAndVariant(namebundle + "bundle", "");
        print(assetPath);
        // fin auxiliar

    }
    */

    public void TargetFound()
    {
        if (!controldescarga.estoyocupado && !controldescarga.panelInstrucciones.activeSelf && !controldescarga.panelSeleccionIdiomaPalabras.activeSelf)
        {
            controldescarga.controlbotones.modopalabras = 2;
            if (yaBaje)
            {
                miHijo.gameObject.SetActive(true);
                miHijo.gameObject.GetComponent<SeguidorARCam>().seguir = false;
                controldescarga.panelSeleccionIdiomaPalabras.SetActive(true);
                controldescarga.target = gameObject;
                controldescarga.controlbotones.abrirBandera();
            }
            else
            {
                string nombre_ab = gameObject.name.ToLower().Replace("_", string.Empty).Replace(" ", string.Empty);
                controldescarga.target = gameObject;
                controldescarga.CargarAssetsLibro(nombre_ab + "bundle");
            }
            controldescarga.panelInstrucciones.SetActive(false);
            List<bool> listaidiomas = new List<bool>();
            listaidiomas.Add(espa�ol);
            listaidiomas.Add(ingles);
            listaidiomas.Add(frances);
            listaidiomas.Add(aleman);
            listaidiomas.Add(italiano);
            listaidiomas.Add(portugues);
            listaidiomas.Add(chino);
            listaidiomas.Add(coreano);
            listaidiomas.Add(arabe);
            controldescarga.controlbotones.ajustarBanderas(listaidiomas);
        }
    }
    public void TargetLost()
    {
        if (!controldescarga.estoyocupado && miHijo != null)
        {
            miHijo.gameObject.AddComponent<SeguidorARCam>();
            miHijo.gameObject.GetComponent<SeguidorARCam>().aSeguir = controldescarga.placeholder;
            miHijo.gameObject.GetComponent<SeguidorARCam>().seguir = true;
            controldescarga.placeholder.transform.localRotation = Quaternion.Euler(-45f, 0f, 0f);
            controldescarga.placeholder.transform.localPosition = new Vector3(0, 0, 400);
            GetAllChildrenYActivarMesh(miHijo.gameObject.transform);
        }
    }
    public void colocarFuncionesAudio()
    {
        for (int i = 0; i < miHijo.transform.GetChild(0).GetChild(0).childCount; i++)
        {
            print(miHijo.transform.GetChild(0).GetChild(0).name);
            Button btn = miHijo.transform.GetChild(0).GetChild(0).GetChild(i).gameObject.GetComponent<Button>();
            BotonAudio btncode = miHijo.transform.GetChild(0).GetChild(0).GetChild(i).gameObject.AddComponent<BotonAudio>();
            btncode.id = i;
            btncode.mitarjeta = gameObject.GetComponent<IdentificadorTarjetaLibro>();
            //btn.onClick.AddListener(delegate { reproducirAudioDeBoton(); });
        }
        if (miHijo.transform.GetChild(0).childCount > 1)
        {
            Button btn = miHijo.transform.GetChild(0).GetChild(1).GetChild(0).gameObject.GetComponent<Button>();
            btn.onClick.AddListener(descargarjuego);
            nombrejuego = miHijo.transform.GetChild(0).GetChild(1).GetChild(0).name;
        }
    }
    public void descargarjuego()
    {
        // descagrgar juego
        if (mijuego != null)
        {
            mijuego.SetActive(true);
        }
        else
        {
            controldescarga.target = gameObject;
            controldescarga.CargarAssetsJuego(nombrejuego+ "bundle");
        }
    }
    public void reproducirAudioDeBoton(int idbotonp)
    {
        actualidboton = idbotonp;
        if (!controldescarga.estoyocupado)
        {
            print("Ya descargue el audio");
            // Colocar Audio
            controldescarga.controlbotones.audioslibros = miHijo.transform.GetChild(0).GetChild(0).GetChild(actualidboton).GetChild(0).GetComponents<AudioSource>();
            controldescarga.controlbotones.reproducirAudioLibros(controldescarga.controlbotones.idiomaactual);
        }
    }
    public void descargarAudios(int idbotonp, string namep)
    {
        actualidboton = idbotonp;
        if (!controldescarga.estoyocupado)
        {
            print("Descargar audio");
            // Descargar
            controldescarga.CargarAssetsAudios(namep, miHijo.transform.GetChild(0).GetChild(0).GetChild(idbotonp).gameObject);
        }
    }
    public void actualizarAudio()
    {
        BotonAudio btncode = miHijo.transform.GetChild(0).GetChild(0).GetChild(actualidboton).gameObject.GetComponent<BotonAudio>();
        btncode.yadescargue = true;
    }
    public void OcultarObjeto()
    {
        DesactivarChildren();
    }
    public void DestroyAllChildren()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
    }
    public void DesactivarChildren()
    {
        miHijo.transform.localPosition = Vector3.zero;
        miHijo.transform.localRotation = Quaternion.identity;
        miHijo.gameObject.SetActive(false); ;
    }
    public void GetAllChildrenYActivarMesh(Transform t)
    {
        List<GameObject> childGOs = new List<GameObject>();
        foreach (Transform childT in t)
        {
            childGOs.Add(childT.gameObject);
            if (childT.childCount > 0)
            {
                GetAllChildrenYActivarMesh(childT);
            }
        }
        foreach (GameObject prueba in childGOs)
        {
            SkinnedMeshRenderer ap;
            if (prueba.TryGetComponent<SkinnedMeshRenderer>(out ap))
            {
                ap.enabled = true;
            }
            MeshRenderer ap2;
            if (prueba.TryGetComponent<MeshRenderer>(out ap2))
            {
                ap2.enabled = true;
            }
            Canvas ap3;
            if (prueba.TryGetComponent<Canvas>(out ap3))
            {
                ap3.enabled = true;
            }
            SpriteRenderer ap4;
            if (prueba.TryGetComponent<SpriteRenderer>(out ap4))
            {
                ap4.enabled = true;
            }
        }
    }
}
