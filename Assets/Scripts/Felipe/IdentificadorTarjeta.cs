using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;
using LitJson;
using System.IO;
using UnityEditor;

public class IdentificadorTarjeta : DefaultObserverEventHandler
{
    public int id;
    public int iddialogos;
    public bool tengodialogo;
    public ControlDescargaAssets controldescarga;
    private string jsonstring;
    private JsonData itemdata;
    [HideInInspector] public bool yaBaje = false, yabajedialogos=false;
    [HideInInspector] public GameObject miHijo = null;
    public bool espa�ol = true, ingles = true, frances = true, aleman = true, italiano = true, portugues = true, chino = true, coreano = true, arabe = true;

    private void Awake()
    {
        base.OnTargetFound.AddListener(TargetFound);
        base.OnTargetLost.AddListener(TargetLost);
    }
    public void TargetFound()
    {
        if (!controldescarga.estoyocupado && !controldescarga.panelInstrucciones.activeSelf && !controldescarga.panelSeleccionIdiomaPalabras.activeSelf)
        {
            controldescarga.controlbotones.modopalabras = 0;
            if (yaBaje)
            {
                miHijo.gameObject.SetActive(true);
                miHijo.gameObject.GetComponent<SeguidorARCam>().seguir = false;
                controldescarga.panelSeleccionIdiomaPalabras.SetActive(true);
                controldescarga.target = gameObject;
                controldescarga.controlbotones.objeto = miHijo;               
                controldescarga.controlbotones.reproducirPalabra(controldescarga.controlbotones.idiomaactual);
                if (tengodialogo)
                {
                    controldescarga.controlbotones.estadoBotonDialogo(true);
                }
                else
                {
                    controldescarga.controlbotones.estadoBotonDialogo(false);
                }
            }
            else
            {
                jsonstring = controldescarga.jsonhud.text;              
                itemdata = JsonMapper.ToObject(jsonstring);
                string prueba = itemdata["palabras"][id]["txtEnglish"].ToString().ToLower() + "bundle";
                prueba.Replace(" ", string.Empty);
                string nombre_ab = prueba.Replace(" ", string.Empty);
                controldescarga.target = gameObject;
                controldescarga.CargarAssets(nombre_ab, id,iddialogos, 1);
                print(nombre_ab);
                controldescarga.panelCarga.SetActive(true);
            }
            controldescarga.panelInstrucciones.SetActive(false);
            List<bool> listaidiomas = new List<bool>();
            listaidiomas.Add(espa�ol);
            listaidiomas.Add(ingles);
            listaidiomas.Add(frances);
            listaidiomas.Add(aleman);
            listaidiomas.Add(italiano);
            listaidiomas.Add(portugues);
            listaidiomas.Add(chino);
            listaidiomas.Add(coreano);
            listaidiomas.Add(arabe);
            controldescarga.controlbotones.ajustarBanderas(listaidiomas);
        }
    }
    public void TargetLost()
    {
        if (!controldescarga.estoyocupado && miHijo != null)
        {
            miHijo.gameObject.AddComponent<SeguidorARCam>();
            miHijo.gameObject.GetComponent<SeguidorARCam>().aSeguir = controldescarga.placeholder;
            miHijo.gameObject.GetComponent<SeguidorARCam>().seguir = true;
            controldescarga.placeholder.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
            controldescarga.placeholder.transform.localPosition = new Vector3(0, -70, 400);
            GetAllChildrenYActivarMesh(miHijo.gameObject.transform);
        }
    }
    public void OcultarObjeto()
    {
        DesactivarChildren();
    }
    public void DestroyAllChildren()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
    }
    public void DesactivarChildren()
    {
        miHijo.transform.localPosition = Vector3.zero;
        miHijo.transform.localRotation = Quaternion.identity;
        miHijo.gameObject.SetActive(false); ;
    }
    public void GetAllChildrenYActivarMesh(Transform t)
    {
        List<GameObject> childGOs = new List<GameObject>();
        foreach (Transform childT in t)
        {
            childGOs.Add(childT.gameObject);
            if (childT.childCount > 0)
            {
                GetAllChildrenYActivarMesh(childT);
            }
        }
        foreach (GameObject prueba in childGOs)
        {
            SkinnedMeshRenderer ap;
            if (prueba.TryGetComponent<SkinnedMeshRenderer>(out ap))
            {
                ap.enabled = true;
            }
            MeshRenderer ap2;
            if (prueba.TryGetComponent<MeshRenderer>(out ap2))
            {
                ap2.enabled = true;
            }
        }
    }
}
