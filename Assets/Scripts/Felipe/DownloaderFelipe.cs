using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.Networking;

public class DownloaderFelipe : MonoBehaviour
{
    public Image barracarga;
    const string BundleFolder = "https://www.mowisolutions.com/mowi/AssetBundles/";
    public void GetBundleObject(string assetName, UnityAction<GameObject> callback, Transform bundleParent)
    {
        StartCoroutine(GetDisplayBundleRoutine(assetName, callback, bundleParent));
    }

    IEnumerator GetDisplayBundleRoutine(string assetName, UnityAction<GameObject> callback, Transform bundleParent)
    {
        string bundleURL = BundleFolder + assetName + "-";

#if UNITY_ANDROID
        bundleURL += "Android";
#else
        bundleURL += "IOS";
#endif

        Debug.Log("Solicitando " + bundleURL);
        using (UnityWebRequest www = UnityWebRequestAssetBundle.GetAssetBundle(bundleURL))
        {
            www.SendWebRequest();
            while (!www.isDone)
            {
                if (www.downloadProgress > 0)
                {
                    barracarga.fillAmount = www.downloadProgress;
                }
                yield return new WaitForSeconds(0.1f);
            }
            if (www.result==UnityWebRequest.Result.ConnectionError)
            {
                Debug.Log("Error de red");
                gameObject.GetComponent<ControlDescargaAssets>().panelCarga.SetActive(false);
                gameObject.GetComponent<ControlDescargaAssets>().panelInstrucciones.SetActive(false);
            }
            else
            {
                print(assetName + " ... " + www);
                AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(www);
                if (bundle != null)
                {
                    string rootAssetPath = bundle.GetAllAssetNames()[0];
                    GameObject arObject = Instantiate(bundle.LoadAsset(rootAssetPath) as GameObject, bundleParent);
                    bundle.Unload(false);
                    callback(arObject);
                }
                else
                {
                    Debug.Log("Asset bundle invalido");
                }
            }
        }
           
    }
}
