using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class ControlDescargaAssets : MonoBehaviour
{
    public DownloaderFelipe downloader;
    [HideInInspector] public GameObject target;
    public ControlBotones controlbotones;
    public GameObject panelInstrucciones, panelCarga, panelSeleccionIdiomaPalabras, contenedorbanderas;
    public GameObject placeholder;
    public Camera cam;
    [HideInInspector] public bool estoyocupado = false;
    public Text textopruebas;
    public TextAsset jsonhud;
    public GameObject canvas;
    public GameObject efecto;

    public int tipodescarga = 0;
    public void CargarAssetsBasic(string nombre, Transform padrep)
    {      
            panelInstrucciones.SetActive(false);
            panelCarga.SetActive(true);
            estoyocupado = true;
            downloader.GetBundleObject(nombre, OnContentLoadedBasic, padrep);
    }
    void OnContentLoadedBasic(GameObject assetp)
    {
        Debug.Log("Se carg� correctamente: " + assetp.name);
        target.GetComponent<IdentificadorPresentacion>().miHijo = assetp;
        target.GetComponent<IdentificadorPresentacion>().yaBaje = true;
        estoyocupado = false;
        panelCarga.SetActive(false);
        downloader.barracarga.fillAmount = 0;
    }

    public void CargarAssets(string nombre, int id, int iddialogep, int tipoDescarga)
    {
        tipodescarga = 0;
        estoyocupado = true;
        if (tipoDescarga == 1)
        {
            controlbotones.txt_textodecarga.text = "Cargando tarjeta\n";
            target.GetComponent<IdentificadorTarjeta>().DestroyAllChildren();
            textopruebas.text = textopruebas.text + "\n>> Llamando al descargador de bundle";
            downloader.GetBundleObject(nombre, OnContentLoaded, target.transform);
            controlbotones.id = id;
            controlbotones.iddialogos = iddialogep;
        }
        else
        {
            panelSeleccionIdiomaPalabras.SetActive(false);
            estoyocupado = true;
            controlbotones.txt_textodecarga.text = "Cargando Audios\n";
            downloader.GetBundleObject(nombre, OnContentLoadedDialogue, target.transform);
        }
    }
    public void CargarAssetsLibro(string nombre)
    {
        tipodescarga = 1;
        estoyocupado = true;
        controlbotones.txt_textodecarga.text = "Cargando p�gina\n";
        panelCarga.SetActive(true);
        downloader.GetBundleObject(nombre, OnContentLoadedLibro, target.transform);
    }
    public void CargarAssetsAudios(string nombre, GameObject padrep)
    {
        estoyocupado = true;
        controlbotones.txt_textodecarga.text = "Cargando audios\n";
        panelCarga.SetActive(true);
        downloader.GetBundleObject(nombre, OnContentLoadedAudio, padrep.transform);
    }
    public void CargarAssetsJuego(string nombre)
    {
        estoyocupado = true;
        controlbotones.txt_textodecarga.text = "Cargando juego\n";
        panelCarga.SetActive(true);
        downloader.GetBundleObject(nombre, OnContentLoadedJuego, canvas.transform);
    }
    void OnContentLoaded(GameObject assetp)
    {
        textopruebas.text = textopruebas.text + "\n>>Se carg� correctamente: " + assetp.name;
        Debug.Log("Se carg� correctamente: " + assetp.name);
        estoyocupado = false;
        panelSeleccionIdiomaPalabras.SetActive(true);
        controlbotones.objeto = assetp;
        controlbotones.reproducirPalabra(controlbotones.idiomaactual);
        if (target.GetComponent<IdentificadorTarjeta>().tengodialogo)
        {
            controlbotones.estadoBotonDialogo(true);
        }
        else
        {
            controlbotones.estadoBotonDialogo(false);
        }
        panelCarga.SetActive(false);
        downloader.barracarga.fillAmount = 0;
        target.GetComponent<IdentificadorTarjeta>().miHijo = assetp;
        target.GetComponent<IdentificadorTarjeta>().yaBaje = true;
        GameObject a = Instantiate(efecto, assetp.transform);
        a.GetComponent<ParticleSystemRenderer>().enabled = true;
        a.transform.localScale = new Vector3(14,14,14);
    }
    void OnContentLoadedDialogue(GameObject assetp)
    {
        Debug.Log("Se carg� correctamente: " + assetp.name);
        estoyocupado = false;
        controlbotones.objetodialogos = assetp;
        panelSeleccionIdiomaPalabras.SetActive(true);
        panelCarga.SetActive(false);
        downloader.barracarga.fillAmount = 0;
        controlbotones.reproducirPalabra(controlbotones.idiomaactual);
        target.GetComponent<IdentificadorTarjeta>().yabajedialogos = true;
    }
    void OnContentLoadedLibro(GameObject assetp)
    {
        Debug.Log("Se carg� correctamente: " + assetp.name);
        estoyocupado = false;
        panelSeleccionIdiomaPalabras.SetActive(true);
        panelCarga.SetActive(false);
        downloader.barracarga.fillAmount = 0;
        target.GetComponent<IdentificadorTarjetaLibro>().miHijo = assetp;
        target.GetComponent<IdentificadorTarjetaLibro>().yaBaje = true;
        target.GetComponent<IdentificadorTarjetaLibro>().colocarFuncionesAudio();
        GameObject a = Instantiate(efecto, assetp.transform);
        a.GetComponent<ParticleSystemRenderer>().enabled = true;
        a.transform.localScale = new Vector3(14, 14, 14);
    }
    void OnContentLoadedAudio(GameObject assetp)
    {
        Debug.Log("Se carg� correctamente: " + assetp.name);
        estoyocupado = false;
        panelSeleccionIdiomaPalabras.SetActive(true);
        panelCarga.SetActive(false);
        downloader.barracarga.fillAmount = 0;
        target.GetComponent<IdentificadorTarjetaLibro>().actualizarAudio();
        target.GetComponent<IdentificadorTarjetaLibro>().reproducirAudioDeBoton(target.GetComponent<IdentificadorTarjetaLibro>().actualidboton);
        controlbotones.abrirBandera();
    }
    void OnContentLoadedJuego(GameObject assetp)
    {
        Debug.Log("Se carg� correctamente: " + assetp.name);
        estoyocupado = false;
        panelSeleccionIdiomaPalabras.SetActive(true);
        panelCarga.SetActive(false);
        downloader.barracarga.fillAmount = 0;
        target.GetComponent<IdentificadorTarjetaLibro>().mijuego = assetp;
    }
    public void cerrarInstrucciones()
    {
        cam.GetComponent<VuforiaBehaviour>().enabled = true;
        panelInstrucciones.SetActive(false);
    }
    public void cerrarPalabra()
    {
        if (tipodescarga == 0)
        {
            target.GetComponent<IdentificadorTarjeta>().DesactivarChildren();
        }
        else if (tipodescarga == 1)
        {
            target.GetComponent<IdentificadorTarjetaLibro>().DesactivarChildren();
        }
        panelSeleccionIdiomaPalabras.SetActive(false);
    }
    public void cerrarTodo()
    {
        if (tipodescarga == 0)
        {
            target.GetComponent<IdentificadorTarjeta>().DesactivarChildren();
        }
        else if(tipodescarga == 1)
        {
            target.GetComponent<IdentificadorTarjetaLibro>().DesactivarChildren();
        }
        panelSeleccionIdiomaPalabras.SetActive(false);
        panelInstrucciones.SetActive(true);
    }

}
