using LitJson;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class CreadorDeAudiosPrefabs : MonoBehaviour
{
   /*
    private string ruta = "Assets/prefabs/AssetsFelipe/AudiosLibro/";
    public TextAsset jsonLibro;
    private string jsonstring;
    private JsonData itemdata;
    // Start is called before the first frame update
    void Start()
    {
        jsonstring = jsonLibro.text;
        itemdata = JsonMapper.ToObject(jsonstring);
       //crearPrefabs();
    }

   public void crearPrefabs()
    {
        for (int i = 0; i < 73; i ++)
        {
            string nombreprefab = itemdata["audios"][i]["audioEnglish"].ToString()
                .ToLower().Replace("_", string.Empty)
                .Replace("in",string.Empty)
                .Replace("en", string.Empty) + "audio";
            GameObject instanciado = new GameObject();
            instanciado.name = nombreprefab;
            for (int j = 0; j< 9; j++)
            {
                AudioSource temp = instanciado.AddComponent<AudioSource>();
                temp.playOnAwake = false;
            }
            AudioSource[] audios = instanciado.GetComponents<AudioSource>();
            print(itemdata["audios"][i]["audioSpanish"].ToString());
            audios[0].clip = Resources.Load<AudioClip>(itemdata["audios"][i]["audioSpanish"].ToString());
            audios[1].clip = Resources.Load<AudioClip>(itemdata["audios"][i]["audioEnglish"].ToString());
            audios[2].clip = Resources.Load<AudioClip>(itemdata["audios"][i]["audioFrench"].ToString());
            audios[3].clip = Resources.Load<AudioClip>(itemdata["audios"][i]["audioGerman"].ToString());
            audios[4].clip = Resources.Load<AudioClip>(itemdata["audios"][i]["audioItalian"].ToString());
            audios[5].clip = Resources.Load<AudioClip>(itemdata["audios"][i]["audiosPortugese"].ToString());
            audios[6].clip = Resources.Load<AudioClip>(itemdata["audios"][i]["audioChinese"].ToString());
            audios[7].clip = Resources.Load<AudioClip>(itemdata["audios"][i]["audioKorean"].ToString());
            audios[8].clip = Resources.Load<AudioClip>(itemdata["audios"][i]["audioArabic"].ToString());

            PrefabUtility.SaveAsPrefabAssetAndConnect(instanciado, ruta + nombreprefab + "bundle" + ".prefab", InteractionMode.UserAction);
           
            AssetImporter.GetAtPath(ruta + nombreprefab + "bundle" + ".prefab").SetAssetBundleNameAndVariant(nombreprefab + "bundle", "");
        }
    }
    */
}
