using LitJson;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class CreadorRuletaPrefabs : MonoBehaviour
{ /*
    private string ruta = "Assets/prefabs/AssetsFelipe/Ruleta/";
    public TextAsset jsonLibro;
    private string jsonstring;
    private JsonData itemdata;
    public GameObject contenedor;
    // Start is called before the first frame update
    void Start()
    {
        jsonstring = jsonLibro.text;
        itemdata = JsonMapper.ToObject(jsonstring);
       crearPrefabs();
    }

   public void crearPrefabs()
    {
        for (int i = 0; i < contenedor.transform.childCount; i ++)
        {
            string nombreprefab = itemdata["palabras"][i]["txtEnglish"].ToString()
                .ToLower().Replace("_", string.Empty).Replace(" ", string.Empty) + "ruleta";
            GameObject instanciado = contenedor.transform.GetChild(i).gameObject;
            instanciado.name = nombreprefab;
            for (int j = 0; j< 11; j++)
            {
                AudioSource temp = instanciado.AddComponent<AudioSource>();
                temp.playOnAwake = false;
            }
            AudioSource[] audios = instanciado.GetComponents<AudioSource>();
            audios[0].clip = Resources.Load<AudioClip>(itemdata["palabras"][i]["audioSpanish"].ToString());
            audios[1].clip = Resources.Load<AudioClip>(itemdata["palabras"][i]["audioEnglish"].ToString());
            audios[2].clip = Resources.Load<AudioClip>(itemdata["palabras"][i]["audioFrench"].ToString());
            audios[3].clip = Resources.Load<AudioClip>(itemdata["palabras"][i]["audioGerman"].ToString());
            audios[4].clip = Resources.Load<AudioClip>(itemdata["palabras"][i]["audioItalian"].ToString());
            audios[5].clip = Resources.Load<AudioClip>(itemdata["palabras"][i]["audiosPortugese"].ToString());
            audios[6].clip = Resources.Load<AudioClip>(itemdata["palabras"][i]["audioChinese"].ToString());
            audios[7].clip = Resources.Load<AudioClip>(itemdata["palabras"][i]["audioKorean"].ToString());
            audios[8].clip = Resources.Load<AudioClip>(itemdata["palabras"][i]["audioArabic"].ToString());
            audios[9].clip = Resources.Load<AudioClip>(itemdata["palabras"][i]["sfx"].ToString());
            audios[10].clip = Resources.Load<AudioClip>(itemdata["palabras"][i]["interactionSfx"].ToString());

            PrefabUtility.SaveAsPrefabAssetAndConnect(instanciado, ruta + nombreprefab + "bundle" + ".prefab", InteractionMode.UserAction);
           
            AssetImporter.GetAtPath(ruta + nombreprefab + "bundle" + ".prefab").SetAssetBundleNameAndVariant(nombreprefab + "bundle", "");
        }
    }*/
    
}
