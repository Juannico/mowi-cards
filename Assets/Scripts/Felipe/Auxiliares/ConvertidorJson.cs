using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
//Felipe
using LitJson;

public class ConvertidorJson : MonoBehaviour
{
    public HUDManager hudmanager;
    public DialogueManager dialoguemanager;
    public BookCode bookcode;
    public Ruleta ruleta;
    ClaseContenedoraTemp clasep = new ClaseContenedoraTemp();
    ClaseContenedoraDialogueTemp clasepdialog = new ClaseContenedoraDialogueTemp();
    ClaseContenedoraBookTemp clasepbook = new ClaseContenedoraBookTemp();
    ClaseContenedoraRuletaTemp clasepruleta = new ClaseContenedoraRuletaTemp();

    private string jsonstring;
    private JsonData itemdata;

    private void Start()
    {
        jsonstring = File.ReadAllText(Application.streamingAssetsPath + "/jsonHUD.json");
        itemdata = JsonMapper.ToObject(jsonstring);
        print("Prueba lectura Json : " + itemdata["audios"][0]["txtSpanish"]);
    }
    public void obtenerStrings()
    {
        // Metodo para individuales (Repeticion)
        List<elementojsonIndividual> listatemp = new List<elementojsonIndividual>();
        for (int i = 0; i < hudmanager.audiosRepeticion.Length; i++)
        {
            elementojsonIndividual aingresar = new elementojsonIndividual();
            aingresar.id = i.ToString();
            aingresar.txtref = hudmanager.audiosRepeticion[i].name;
            listatemp.Add(aingresar);
        }
        clasep.audiosRepeticion = listatemp.ToArray();       
        // Metodo para audios
        List<elementojsonPalabra> listatemp2 = new List<elementojsonPalabra>();
        for (int i = 0; i < hudmanager.textosSpanish.Length; i++)
        {
            elementojsonPalabra aingresar = new elementojsonPalabra();
            aingresar.id = i.ToString();
            //
            aingresar.txtSpanish = hudmanager.textosSpanish[i];
            aingresar.audioSpanish = hudmanager.audiosSpanish[i].name;
            //
            aingresar.txtEnglish = hudmanager.textosEnglish[i];
            aingresar.audioEnglish = hudmanager.audiosEnglish[i].name;
            //
            aingresar.txtFrench = hudmanager.textosFrench[i];
            aingresar.audioFrench = hudmanager.audiosFrench[i].name;
            //
            aingresar.txtGerman = hudmanager.textosGerman[i];
            aingresar.audioGerman = hudmanager.audiosGerman[i].name;
            //
            aingresar.txtChinese = hudmanager.textosChinese[i];
            aingresar.audioChinese = hudmanager.audiosChinese[i].name;
            //
            aingresar.txtsPortugese = hudmanager.textosPortugese[i];
            aingresar.audiosPortugese = hudmanager.audiosPortugese[i].name;
            //
            aingresar.txtItalian = hudmanager.textosItalian[i];
            aingresar.audioItalian = hudmanager.audiosItalian[i].name;
            //
            aingresar.txtArabic = hudmanager.textosArabic[i];
            aingresar.audioArabic = hudmanager.audiosArabic[i].name;
            //
            aingresar.txtKorean = hudmanager.textosKorean[i];
            aingresar.audioKorean = hudmanager.audiosKorean[i].name;
            //
            aingresar.txtPKorean = hudmanager.textosPKorean[i];
            aingresar.txtPArabic = hudmanager.textosPArabic[i];
            aingresar.txtPChinese = hudmanager.textosPChinese[i];
            //
            aingresar.sfx = hudmanager.sfx[i].name;
            aingresar.interactionSfx = hudmanager.interactionSfx[i].name;

            listatemp2.Add(aingresar);
        }
        clasep.audios = listatemp2.ToArray();
    }
   public void guardarJson()
    {
        string json = JsonUtility.ToJson(clasep, true);
        File.WriteAllText(Application.dataPath + "/jsonHUD.json", json);
    }


    public void obtenerStringsDialogue()
    {
        // Metodo para audios
        List<elementojsonDiaolog> listatemp2 = new List<elementojsonDiaolog>();
        for (int i = 0; i < dialoguemanager.textosSpanish.Length; i++)
        {
            elementojsonDiaolog aingresar = new elementojsonDiaolog();
            aingresar.id = i.ToString();
            //
            aingresar.txtSpanish = dialoguemanager.textosSpanish[i];
            aingresar.audioSpanish = dialoguemanager.audiosSpanish[i].name;
            //
            aingresar.txtEnglish = dialoguemanager.textosEnglish[i];
            aingresar.audioEnglish = dialoguemanager.audiosEnglish[i].name;
            //
            aingresar.txtFrench = dialoguemanager.textosFrench[i];
            aingresar.audioFrench = dialoguemanager.audiosFrench[i].name;
            //
            aingresar.txtGerman = dialoguemanager.textosGerman[i];
            aingresar.audioGerman = dialoguemanager.audiosGerman[i].name;
            //
            aingresar.txtChinese = dialoguemanager.textosChinese[i];
            aingresar.audioChinese = dialoguemanager.audiosChinese[i].name;
            //
            aingresar.txtsPortugese = dialoguemanager.textosPortugese[i];
            aingresar.audiosPortugese = dialoguemanager.audiosPortugese[i].name;
            //
            aingresar.txtItalian = dialoguemanager.textosItalian[i];
            aingresar.audioItalian = dialoguemanager.audiosItalian[i].name;
            //
            aingresar.txtArabic = dialoguemanager.textosArabic[i];
            aingresar.audioArabic = dialoguemanager.audiosArabic[i].name;
            //
            aingresar.txtKorean = dialoguemanager.textosKorean[i];
            aingresar.audioKorean = dialoguemanager.audiosKorean[i].name;
            //
            aingresar.txtPKorean = dialoguemanager.textosPKorean[i];
            aingresar.txtPArabic = dialoguemanager.textosPArabic[i];
            aingresar.txtPChinese = dialoguemanager.textosPChinese[i];
           

            listatemp2.Add(aingresar);
        }
        clasepdialog.dialogos = listatemp2.ToArray();
    }
    public void guardarJsonDialogue()
    {
        string json = JsonUtility.ToJson(clasepdialog, true);
        File.WriteAllText(Application.dataPath + "/jsonDialogue.json", json);
    }



    public void obtenerStringBook()
    {
        // Metodo para audios
        List<elementojsonBook> listatemp2 = new List<elementojsonBook>();
        for (int i = 0; i < bookcode.audiosSpanish.Length; i++)
        {
            elementojsonBook aingresar = new elementojsonBook();
            aingresar.id = i.ToString();
            //
            aingresar.audioSpanish = bookcode.audiosSpanish[i].name;
            //
            aingresar.audioEnglish = bookcode.audiosEnglish[i].name;
            //
            aingresar.audioFrench = bookcode.audiosFrench[i].name;
            //
            aingresar.audioGerman = bookcode.audiosGerman[i].name;
            //
            aingresar.audioChinese = bookcode.audiosChinese[i].name;
            //
            aingresar.audiosPortugese = bookcode.audiosPortugese[i].name;
            //
            aingresar.audioItalian = bookcode.audiosItalian[i].name;
            //
            aingresar.audioArabic = bookcode.audiosArabic[i].name;
            //
            aingresar.audioKorean = bookcode.audiosKorean[i].name;
     


            listatemp2.Add(aingresar);
        }
        clasepbook.audios = listatemp2.ToArray();
    }
    public void guardarJsonBook()
    {
        string json = JsonUtility.ToJson(clasepbook, true);
        File.WriteAllText(Application.dataPath + "/jsonBook.json", json);
    }


    public void obtenerStringsRuleta()
    {
        // Metodo para audios
        List<elementojsonRuleta> listatemp2 = new List<elementojsonRuleta>();
        for (int i = 0; i < ruleta.textosSpanish.Length; i++)
        {
            elementojsonRuleta aingresar = new elementojsonRuleta();
            aingresar.id = i.ToString();
            //
            aingresar.txtSpanish = ruleta.textosSpanish[i];
            aingresar.audioSpanish = ruleta.audiosSpanish[i].name;
            //
            aingresar.txtEnglish = ruleta.textosEnglish[i];
            aingresar.audioEnglish = ruleta.audiosEnglish[i].name;
            //
            aingresar.txtFrench = ruleta.textosFrench[i];
            aingresar.audioFrench = ruleta.audiosFrench[i].name;
            //
            aingresar.txtGerman = ruleta.textosGerman[i];
            aingresar.audioGerman = ruleta.audiosGerman[i].name;
            //
            aingresar.txtChinese = ruleta.textosChinese[i];
            aingresar.audioChinese = ruleta.audiosChinese[i].name;
            //
            aingresar.txtsPortugese = ruleta.textosPortugese[i];
            aingresar.audiosPortugese = ruleta.audiosPortugese[i].name;
            //
            aingresar.txtItalian = ruleta.textosItalian[i];
            aingresar.audioItalian = ruleta.audiosItalian[i].name;
            //
            aingresar.txtArabic = ruleta.textosArabic[i];
            aingresar.audioArabic = ruleta.audiosArabic[i].name;
            //
            aingresar.txtKorean = ruleta.textosKorean[i];
            aingresar.audioKorean = ruleta.audiosKorean[i].name;
            //
            aingresar.txtPKorean = ruleta.textosPKorean[i];
            aingresar.txtPArabic = ruleta.textosPArabic[i];
            aingresar.txtPChinese = ruleta.textosPChinese[i];
            //
            aingresar.sfx = ruleta.sfx[i].name;
            aingresar.interactionSfx = ruleta.interactionSfx[i].name;

            listatemp2.Add(aingresar);
        }
        clasepruleta.audios = listatemp2.ToArray();
    }
    public void guardarJsonRuleta()
    {
        string json = JsonUtility.ToJson(clasepruleta, true);
        File.WriteAllText(Application.dataPath + "/jsonRULETA.json", json);
    }

}
