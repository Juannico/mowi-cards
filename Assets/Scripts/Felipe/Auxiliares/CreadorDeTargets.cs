using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreadorDeTargets : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
        gameObject.AddComponent<IdentificadorTarjetaLibro>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
