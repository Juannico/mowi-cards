using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeguidorARCam : MonoBehaviour
{
    public bool seguir = true;
    public GameObject aSeguir = null;
    // Update is called once per frame
    void Update()
    {
        if (seguir && aSeguir != null)
        {
            gameObject.transform.position = aSeguir.transform.position;
            transform.rotation = aSeguir.transform.rotation;
        }
    }
}
