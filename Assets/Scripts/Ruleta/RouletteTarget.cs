﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class RouletteTarget : DefaultObserverEventHandler
{
    //private TrackableBehaviour mTrackableBehaviour;
    public GameObject rouletteManager;
    public int thisWord;
    public GameObject model;
    public MeshRenderer[] myMeshes;
    public SkinnedMeshRenderer[] mySkinned;
    GameObject placeHolder;
    public static bool close;
    [HideInInspector] public int state;
    public Transform startPos;
    GameObject[] models;
    public ParticleSystem particle;
    // Start is called before the first frame update
    void Start()
    {
        models = GameObject.FindGameObjectsWithTag("Player");
        model.SetActive(false);
        model.transform.position = startPos.position;
        state = 0;//0 en posicion inicial, 1 en posicion inicial, 2 en placeholder, 3 en pos inicial
        close = false;
        placeHolder = GameObject.Find("PlaceHolder");
        //mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        /*if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }*/
    }

    // Update is called once per frame
    void Update()
    {
        if (state == 0 || state == 1)
        {
            model.transform.position = startPos.position;
        }
        else if (state == 2)
        {
            model.transform.position = placeHolder.transform.position;
            foreach (MeshRenderer mesh in model.GetComponentsInChildren<MeshRenderer>())
            {
                mesh.enabled = true;
            }
            foreach (SkinnedMeshRenderer skinmesh in model.GetComponentsInChildren<SkinnedMeshRenderer>())
            {
                skinmesh.enabled = true;
            }
        }

        if (close)
        {
            foreach (GameObject ob in models)
            {
                ob.SetActive(false);
            }
            for (int i = 0; i < myMeshes.Length; i++)
            {
                myMeshes[i].enabled = false;
            }
            for (int i = 0; i < mySkinned.Length; i++)
            {
                mySkinned[i].enabled = false;
            }
            /*model.SetActive(false);
            foreach (MeshRenderer mesh in model.GetComponentsInChildren<MeshRenderer>())
            {
                mesh.enabled = false;
            }
            foreach (SkinnedMeshRenderer skinmesh in model.GetComponentsInChildren<SkinnedMeshRenderer>())
            {
                skinmesh.enabled = false;
            }*/
            rouletteManager.GetComponent<Ruleta>().isActive = false;
            state = 0;

            close = false;

        }
        if (rouletteManager.GetComponent<Ruleta>().interactParticles)
        {
            Debug.Log("play particles");
            particle.Stop();
            particle.Play();
            rouletteManager.GetComponent<Ruleta>().interactParticles = false;
        }
    }

    /*public void OnTrackableStateChanged(
                                    TrackableBehaviour.Status previousStatus,
                                    TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            Debug.Log("target found trackable state");
            model.transform.position = startPos.position;
            rouletteManager.GetComponent<Ruleta>().targetID = thisWord;
            rouletteManager.GetComponent<Ruleta>().isActive = true;
            model.SetActive(true);
            for (int i = 0; i < myMeshes.Length; i++)
            {
                myMeshes[i].enabled = true;
            }
            for (int i = 0; i < mySkinned.Length; i++)
            {
                mySkinned[i].enabled = true;
            }
            state = 1;

        }
        else
        {
            if (state == 1)
            {
                state = 2;
            }

        }
    }*/

    public void TargetFound()
    {
        Debug.Log("target found function");
        model.transform.position = startPos.position;
        rouletteManager.GetComponent<Ruleta>().targetID = thisWord;
        rouletteManager.GetComponent<Ruleta>().isActive = true;
        model.SetActive(true);
        for (int i = 0; i < myMeshes.Length; i++)
        {
            myMeshes[i].enabled = true;
        }
        for (int i = 0; i < mySkinned.Length; i++)
        {
            mySkinned[i].enabled = true;
        }
        state = 1;
    }

    public void TargetLost()
    {
        if (state == 1)
        {
            state = 2;
        }
    }
}
