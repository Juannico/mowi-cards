﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Vuforia;

public class Ruleta : MonoBehaviour
{
    int random, randomLanguage;
    float timeInterval;
    bool isCoroutine/*, fromCoroutineToUpdate*/;
    int finalAngle;
    float totalAngle;
    public Text winText, pronunctiation;
    public int section, currentIDNumber;
    int clearWord;
    [HideInInspector] public bool isActive;
    [HideInInspector] public bool interactParticles = false;
    [HideInInspector] public int targetID;
    bool searchEnabled, canPressBtn;
    int languageIDForRepeat;

    bool hasPlayed_1 = false;
    bool hasPlayed_2 = false;
    bool playInUpdate = false;
    bool playInteraction = false;
    //public string[] prizeName;
    public GameObject ruleta, languagePanel, correct, incorrect, uiruleta, correctLang, incorrectLang, guessLang, flags, repeat, chosenFlag;
    public AudioClip[] audiosRepeticion;
    public string[] textosSpanish;
    public AudioClip[] audiosSpanish;
    public AudioClip[] audiosEnglish;
    public string[] textosEnglish;
    public AudioClip[] audiosFrench;
    public string[] textosFrench;
    public AudioClip[] audiosGerman;
    public string[] textosGerman;
    public AudioClip[] audiosChinese;
    public string[] textosChinese;
    public string[] textosPChinese;
    public AudioClip[] audiosPortugese;
    public string[] textosPortugese;
    public AudioClip[] audiosItalian;
    public string[] textosItalian;
    public AudioClip[] audiosArabic;
    public string[] textosArabic;
    public string[] textosPArabic;
    public AudioClip[] audiosKorean;
    public string[] textosKorean;
    public string[] textosPKorean;
    public AudioClip[] sfx;
    public AudioClip[] interactionSfx;
    public Sprite[] banderas;
    public AudioSource speaker, sfxsource, interactionSound;
    // Start is called before the first frame update
    void Start()
    {
        VuforiaBehaviour.Instance.enabled = false;
        canPressBtn = false;
        languagePanel.SetActive(false);
        targetID = -1;
        searchEnabled = false;
        clearWord = 0;
        pronunctiation.text = "";
        winText.text = "";
        isCoroutine = true;
        //fromCoroutineToUpdate = false;
        totalAngle = 360 / section;
        correct.SetActive(false);
        incorrect.SetActive(false);
        correctLang.SetActive(false);
        incorrectLang.SetActive(false);
        guessLang.SetActive(false);
        chosenFlag.SetActive(false);
        flags.SetActive(false);
        repeat.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (searchEnabled)
        {
            VuforiaBehaviour.Instance.enabled = true;
            if (isActive)
            {
                //si prendí el marcador
                if (currentIDNumber == targetID)
                {
                    Debug.Log("id actual " + currentIDNumber + "target" + targetID);
                    //si mi palabra elegida es igual al marcador en donde estoy, la gente puede adivinar el idioma prendiendo las banderas
                    guessLang.SetActive(true);
                    correct.SetActive(true);
                    flags.SetActive(true);
                    repeat.SetActive(true);
                }
                else
                {
                    Debug.Log("id actual " + currentIDNumber + "target" + targetID);
                    //si no, la gente solo puede ponerse a buscar otra tarjeta
                    incorrect.SetActive(true);
                    canPressBtn = true;
                }
                if (clearWord <= 1)
                    clearWord = 1;
                languagePanel.SetActive(true);
                sfxsource.clip = sfx[currentIDNumber];
                if (!sfxsource.isPlaying)
                    sfxsource.Play();
            }
            else
            {
                clearWord = 0;
                languagePanel.SetActive(false);
                sfxsource.Stop();
            }

            /*if (clearWord == 1)
            {
                winText.text = "";
                pronunctiation.text = "";
            }*/

            if (playInUpdate)
            {
                if (!hasPlayed_1)
                {
                    speaker.clip = audiosRepeticion[languageIDForRepeat];
                    speaker.Play();
                    hasPlayed_1 = true;
                }
                if (hasPlayed_1 && !hasPlayed_2 && !speaker.isPlaying)
                {
                    if (languageIDForRepeat == 0)
                        speaker.clip = audiosSpanish[currentIDNumber];
                    if (languageIDForRepeat == 1)
                        speaker.clip = audiosEnglish[currentIDNumber];
                    if (languageIDForRepeat == 2)
                        speaker.clip = audiosFrench[currentIDNumber];
                    if (languageIDForRepeat == 3)
                        speaker.clip = audiosChinese[currentIDNumber];
                    if (languageIDForRepeat == 4)
                        speaker.clip = audiosKorean[currentIDNumber];
                    if (languageIDForRepeat == 5)
                        speaker.clip = audiosArabic[currentIDNumber];
                    if (languageIDForRepeat == 6)
                        speaker.clip = audiosPortugese[currentIDNumber];
                    if (languageIDForRepeat == 7)
                        speaker.clip = audiosItalian[currentIDNumber];
                    if (languageIDForRepeat == 8)
                        speaker.clip = audiosGerman[currentIDNumber];
                    speaker.Play();
                    hasPlayed_2 = true;
                }
                if (hasPlayed_1 && hasPlayed_2 && !speaker.isPlaying)
                {
                    playInUpdate = false;
                    hasPlayed_1 = false;
                    hasPlayed_2 = false;
                }

                if (playInteraction)
                {
                    playInteraction = false;
                    interactParticles = true;
                    interactionSound.clip = interactionSfx[currentIDNumber];
                    if (!interactionSound.isPlaying)
                        interactionSound.Play();
                }
            }
        }
        else
        {
            RouletteTarget.close = true;
            VuforiaBehaviour.Instance.enabled = false;
        }
    }

    public void Botones(string type)
    {
        switch (type)
        {
            case "spin":
                if (isCoroutine)
                {
                    StartCoroutine(Spin());
                }
                break;
            case "repeatAfterMe":
                playInUpdate = true;
                break;
            case "interact":
                playInteraction = true;
                break;
            case "bad":
                if (canPressBtn)
                {
                    searchEnabled = true;
                    languagePanel.SetActive(false);
                    correct.SetActive(false);
                    incorrect.SetActive(false);
                    correctLang.SetActive(false);
                    incorrectLang.SetActive(false);
                    isActive = false;
                    canPressBtn = false;
                }
                //según el segmento que saqué y el idioma que saqué tengo que encontrar la tarjeta que corresponde (section) 
                //y marcar el idioma que corresponde (languageIDForRepeat)
                //debe haber instrucciones en algún momento
                break;
            case "good":
                if (canPressBtn)
                {
                    searchEnabled = false;
                    ruleta.SetActive(true);
                    uiruleta.SetActive(true);
                    languagePanel.SetActive(false);
                    correct.SetActive(false);
                    incorrect.SetActive(false);
                    correctLang.SetActive(false);
                    incorrectLang.SetActive(false);
                    isActive = false;
                    canPressBtn = false;
                }
                break;
            case "search":
                searchEnabled = true;
                languagePanel.SetActive(false);
                correct.SetActive(false);
                incorrect.SetActive(false);
                correctLang.SetActive(false);
                incorrectLang.SetActive(false);
                ruleta.SetActive(false);
                uiruleta.SetActive(false);
                break;
            case "close":
                SceneManager.LoadScene(0);
                break;
            case "closetarget":
                RouletteTarget.close = true;
                break;
        }
    }

    public void BotonesIdioma(string idioma)
    {
        clearWord = 2;
        switch (idioma)
        {
            case "spanish":
                languageIDForRepeat = 0;
                break;
            case "english":
                languageIDForRepeat = 1;
                break;
            case "french":
                languageIDForRepeat = 2;
                break;
            case "chinese":
                languageIDForRepeat = 4;
                break;
            case "korean":
                languageIDForRepeat = 8;
                break;
            case "arabic":
                languageIDForRepeat = 7;
                break;
            case "portugese":
                languageIDForRepeat = 5;
                break;
            case "italian":
                languageIDForRepeat = 6;
                break;
            case "german":
                languageIDForRepeat = 3;
                break;
        }
        if(languageIDForRepeat == randomLanguage)
        {
            incorrectLang.SetActive(false);
            correctLang.SetActive(true);
            speaker.Play();
            canPressBtn = true;
            guessLang.GetComponent<Text>().text = "Bien";
        }
        else
        {
            incorrectLang.SetActive(true);
            guessLang.GetComponent<Text>().text = "Intenta de nuevo";
        }
    }

    IEnumerator Spin()
    {
        isCoroutine = false;
        random = Random.Range(200, 300);
        timeInterval = 0.0001f * 2 * Time.deltaTime;
        for (int i= 0; i<random; i++)
        {
            ruleta.transform.Rotate(0, 0, totalAngle / 2);//inicio de rotación
            //desaceleración
            if (i > Mathf.RoundToInt(random * 0.2f))
            {
                timeInterval = 0.5f * Time.deltaTime;
            }
            if (i > Mathf.RoundToInt(random * 0.5f))
            {
                timeInterval = 1f * Time.deltaTime;
            }
            if (i > Mathf.RoundToInt(random * 0.7f))
            {
                timeInterval = 1.5f * Time.deltaTime;
            }
            if (i > Mathf.RoundToInt(random * 0.8f))
            {
                timeInterval = 2f * Time.deltaTime;
            }
            if (i > Mathf.RoundToInt(random * 0.9f))
            {
                timeInterval = 2.5f * Time.deltaTime;
            }
            yield return new WaitForSeconds(timeInterval);
        }
        //yield return new  WaitForSeconds(timeInterval);

        if (Mathf.RoundToInt(ruleta.transform.eulerAngles.z) % totalAngle != 0)//por si cae entre dos números
        {
            ruleta.transform.Rotate(0, 0, totalAngle / 2);
        }
        finalAngle = Mathf.RoundToInt(ruleta.transform.eulerAngles.z);//redondeo del ángulo
        for(int i = 0; i < section; i++)
        {
            //yield return new WaitForSeconds(timeInterval);
            //Debug.Log("ángulo total por i" + i * totalAngle);
            if (finalAngle == i * totalAngle)
            {
                currentIDNumber = i;
                randomLanguage = Random.Range(0, 10);
                chosenFlag.SetActive(true);
                chosenFlag.GetComponent<UnityEngine.UI.Image>().sprite = banderas[randomLanguage];
                if (randomLanguage == 0)
                {
                    speaker.clip = audiosSpanish[i];
                    winText.text = textosSpanish[i];
                    pronunctiation.text = "";
                }
                else if (randomLanguage == 1)
                {
                    speaker.clip = audiosEnglish[i];
                    winText.text = textosEnglish[i];
                    pronunctiation.text = "";
                }
                else if (randomLanguage == 2)
                {
                    speaker.clip = audiosFrench[i];
                    winText.text = textosFrench[i];
                    pronunctiation.text = "";
                }
                else if (randomLanguage == 3)
                {
                    speaker.clip = audiosGerman[i];
                    winText.text = textosGerman[i];
                    pronunctiation.text = "";
                }
                else if (randomLanguage == 4)
                {
                    speaker.clip = audiosChinese[i];
                    winText.text = textosChinese[i];
                    pronunctiation.text = textosPChinese[i];
                }
                else if (randomLanguage == 5)
                {
                    speaker.clip = audiosPortugese[i];
                    winText.text = textosPortugese[i];
                    pronunctiation.text = "";
                }
                else if (randomLanguage == 6)
                {
                    speaker.clip = audiosItalian[i];
                    winText.text = textosItalian[i];
                    pronunctiation.text = "";
                }
                else if (randomLanguage == 7)
                {
                    speaker.clip = audiosArabic[i];
                    winText.text = textosArabic[i];
                    pronunctiation.text = textosPArabic[i];
                }
                else if (randomLanguage == 8)
                {
                    speaker.clip = audiosKorean[i];
                    winText.text = textosKorean[i];
                    pronunctiation.text = textosPKorean[i];
                }
                speaker.Play();
                //winText.text = prizeName[i];
            }
        }
        isCoroutine = true;
    }
}
