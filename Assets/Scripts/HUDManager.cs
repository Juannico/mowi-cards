﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;
using UnityEngine.SceneManagement;

public class HUDManager : MonoBehaviour
{
    public GameObject instrucciones;
    public Camera cam;
    public AudioSource language, sfxsource, interactionSound;
    public Text word, pronunctiation;
    [HideInInspector] public int wordID;
    public GameObject languagePanel;
    [HideInInspector] public bool isActive;
    [HideInInspector] public bool interactParticles = false; 

    public AudioClip[] audiosRepeticion;
    public AudioClip[] sfx;
    public AudioClip[] interactionSfx;
    public AudioClip[] audiosSpanish;
    public string[] textosSpanish;
    public AudioClip[] audiosEnglish;
    public string[] textosEnglish;
    public AudioClip[] audiosFrench;
    public string[] textosFrench;
    public AudioClip[] audiosGerman;
    public string[] textosGerman;
    public AudioClip[] audiosChinese;
    public string[] textosChinese;
    public string[] textosPChinese;
    public AudioClip[] audiosPortugese;
    public string[] textosPortugese;
    public AudioClip[] audiosItalian;
    public string[] textosItalian;
    public AudioClip[] audiosArabic;
    public string[] textosArabic;
    public string[] textosPArabic;
    public AudioClip[] audiosKorean;
    public string[] textosKorean;
    public string[] textosPKorean;

    int clearWord;
    int languageIDForRepeat;

    bool hasPlayed_1 = false;
    bool hasPlayed_2 = false;
    bool playInUpdate = false;
    bool playInteraction = false;

    // Start is called before the first frame update
    void Start()
    {
        //cam.GetComponent<VuforiaBehaviour>().enabled = false;
        clearWord = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("clear word state " + clearWord);
        if (isActive)
        {
            //Debug.Log("is active");
            if (clearWord <= 1)
                clearWord = 1;
            languagePanel.SetActive(true);
            sfxsource.clip = sfx[wordID];
            if (!sfxsource.isPlaying)
                sfxsource.Play();
            //language.clip = audiosSpanish[wordID];
            //language.Play();
        }
        else
        {
            //Debug.Log("is not active");
            clearWord = 0;
            languagePanel.SetActive(false);
            sfxsource.Stop();
        }
        if (clearWord == 1)
        {
            word.text = "";
            pronunctiation.text = "";
        }
        if (playInUpdate)
        {
            if (!hasPlayed_1)
            {
                language.clip = audiosRepeticion[languageIDForRepeat];
                language.Play();
                hasPlayed_1 = true;
            }
            if (hasPlayed_1 && !hasPlayed_2 && !language.isPlaying)
            {
                if (languageIDForRepeat == 0)
                    language.clip = audiosSpanish[wordID];
                if (languageIDForRepeat == 1)
                    language.clip = audiosEnglish[wordID];
                if (languageIDForRepeat == 2)
                    language.clip = audiosFrench[wordID];
                if (languageIDForRepeat == 3)
                    language.clip = audiosChinese[wordID];
                if (languageIDForRepeat == 4)
                    language.clip = audiosKorean[wordID];
                if (languageIDForRepeat == 5)
                    language.clip = audiosArabic[wordID];
                if (languageIDForRepeat == 6)
                    language.clip = audiosPortugese[wordID];
                if (languageIDForRepeat == 7)
                    language.clip = audiosItalian[wordID];
                if (languageIDForRepeat == 8)
                    language.clip = audiosGerman[wordID];
                language.Play();
                hasPlayed_2 = true;
            }
            if (hasPlayed_1 && hasPlayed_2 && !language.isPlaying)
            {
                playInUpdate = false;
                hasPlayed_1 = false;
                hasPlayed_2 = false;
            }
        }
        if (playInteraction)
        {
            playInteraction = false;
            interactParticles = true;
            interactionSound.clip = interactionSfx[wordID];
            if (!interactionSound.isPlaying)
                interactionSound.Play();
        }
    }

    public void OtherUIButtons(string tipo)
    {
        switch (tipo)
        {
            case "close":
                cam.GetComponent<VuforiaBehaviour>().enabled = true;
                instrucciones.SetActive(false);
                break;
            case "repeatAfterMe":
                playInUpdate = true;
                break;
            case "closeHUD":
                //Debug.Log("fucking close");
                VuforiaTrackManager.close = true;
                break;
            case "interact":
                playInteraction = true;
                break;
            case "home":
                SceneManager.LoadScene(0);
                break;
        }
    }

    public void BotonesIdioma (string idioma)
    {
        clearWord = 2;
        switch (idioma)
        {
            case "spanish":
                languageIDForRepeat = 0;
                pronunctiation.text = "";
                language.clip = audiosSpanish[wordID];
                word.text = textosSpanish[wordID];
                break;
            case "english":
                languageIDForRepeat = 1;
                pronunctiation.text = "";
                language.clip = audiosEnglish[wordID];
                word.text = textosEnglish[wordID];
                break;
            case "french":
                languageIDForRepeat = 2;
                pronunctiation.text = "";
                language.clip = audiosFrench[wordID];
                word.text = textosFrench[wordID];
                break;
            case "chinese":
                languageIDForRepeat = 3;
                pronunctiation.text = textosPChinese[wordID];
                language.clip = audiosChinese[wordID];
                word.text = textosChinese[wordID];
                break;
            case "korean":
                languageIDForRepeat = 4;
                pronunctiation.text = textosPKorean[wordID];
                language.clip = audiosKorean[wordID];
                word.text = textosKorean[wordID];
                break;
            case "arabic":
                languageIDForRepeat = 5;
                pronunctiation.text = textosPArabic[wordID];
                language.clip = audiosArabic[wordID];
                word.text = textosArabic[wordID];
                break;
            case "portugese":
                languageIDForRepeat = 6;
                pronunctiation.text = "";
                language.clip = audiosPortugese[wordID];
                word.text = textosPortugese[wordID];
                break;
            case "italian":
                languageIDForRepeat = 7;
                pronunctiation.text = "";
                language.clip = audiosItalian[wordID];
                word.text = textosItalian[wordID];
                break;
            case "german":
                languageIDForRepeat = 8;
                pronunctiation.text = "";
                language.clip = audiosGerman[wordID];
                word.text = textosGerman[wordID];
                break;
        }
        language.Play();
    }
}
