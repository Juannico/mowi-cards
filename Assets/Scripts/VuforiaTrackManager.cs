﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
//using UnityEngine.UI;

public class VuforiaTrackManager : DefaultObserverEventHandler
{
    //private TrackableBehaviour mTrackableBehaviour;
    //public AudioSource speaker;
    public HUDManager hUDManager;
    public int thisWord;
    public GameObject model;
    public MeshRenderer[] myMeshes;
    public SkinnedMeshRenderer[] mySkinned;
    GameObject placeHolder;
    public static bool close;
    [HideInInspector] public int state;
   // public Transform startPos;
    GameObject[] models;
    public ParticleSystem particle;
    // Start is called before the first frame update
    void Start()
    {
        models = GameObject.FindGameObjectsWithTag("Player");
        model.SetActive(false);
        //model.transform.position = startPos.position;
        state = 0;//0 en posicion inicial, 1 en posicion inicial, 2 en placeholder, 3 en pos inicial
        close = false;
        placeHolder = GameObject.Find("PlaceHolder"); 
    }

    // Update is called once per frame
    void Update()
    {
        if (state == 0 || state == 1)
        {
            //model.transform.position = startPos.position;
        }
        else if (state == 2)
        {
            model.transform.position = placeHolder.transform.position;
            foreach (MeshRenderer mesh in model.GetComponentsInChildren<MeshRenderer>())
            {
                mesh.enabled = true;
            }
            foreach (SkinnedMeshRenderer skinmesh in model.GetComponentsInChildren<SkinnedMeshRenderer>())
            {
                skinmesh.enabled = true;
            }
        }

        if (close)
        {
            foreach (GameObject ob in models)
            {
                ob.SetActive(false);
            }
            model.SetActive(false);
            for (int i = 0; i < myMeshes.Length; i++)
            {
                myMeshes[i].enabled = false;
            }
            for (int i = 0; i < mySkinned.Length; i++)
            {
                mySkinned[i].enabled = false;
            }
           
            state = 0;
            
            hUDManager.isActive = false;
            close = false;
            
        }

        if (hUDManager.interactParticles)
        {
            Debug.Log("play particles");
            particle.Stop();
            particle.Play();
            hUDManager.interactParticles = false;
        }
    }

    public void TargetFound()
    {
        //model.transform.position = startPos.position;
        // Play audio when target is found
        hUDManager.wordID = thisWord;
        hUDManager.isActive = true;
        model.SetActive(true);
        for (int i = 0; i < myMeshes.Length; i++)
        {
            myMeshes[i].enabled = true;
        }
        for (int i = 0; i < mySkinned.Length; i++)
        {
            mySkinned[i].enabled = true;
        }
        state = 1;
    }

    public void TargetLost()
    {
        if (state == 1)
        {
            state = 2;
        }
    }
}
