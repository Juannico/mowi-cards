﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game2Script : MonoBehaviour
{
    public GameObject win, lose, gameover, container;
    public GameObject[] buttons;
    public GameObject[] textButtons;
    int id, endcounter, wordid;
    bool firstSelected;
    public TutorialSystem tutorial;

    public AudioSource celebracion_sonido;
    public ParticleSystem particulas;
    // Start is called before the first frame update
    void Start()
    {
        celebracion_sonido = particulas.GetComponent<AudioSource>();
        win.SetActive(false);
        lose.SetActive(false);
        id = -1;
        wordid = -1;
        endcounter = 0;
        firstSelected = false;
        gameover.SetActive(true);
        lanzarTuto();
    }
    public void lanzarTuto()
    {
        List<string> lista_instrucciones = new List<string>();
        lista_instrucciones.Add("Hola! vamos a jugar mientras aprendemos");
        lista_instrucciones.Add("Selecciona la imagen y texto que corresponda");
        tutorial.actualizarListaInstrucciones(lista_instrucciones);
        tutorial.abrirVentana();
    }
    private void OnEnable()
    {
        win.SetActive(false);
        lose.SetActive(false);
        id = -1;
        wordid = -1;
        endcounter = 0;
        firstSelected = false;
        gameover.SetActive(true);
        lanzarTuto();
    }

    // Update is called once per frame
    void Update()
    {
        if (endcounter == buttons.Length)
        {
            reiniciarBotones();
            endcounter = 0;
            gameover.SetActive(true);
        }
    }

    void IconMethod(int iconid)
    {
        if (buttons[iconid].GetComponent<ButtonScript>().state != 2)//si no me lo gasté
        {
            if (!firstSelected)//no busco pareja aún
            {
                //id = iconid;//seteo mi id porque no estoy buscando pareja
                firstSelected = true;//activo mi búsqueda de pareja
                buttons[iconid].GetComponent<ButtonScript>().state = 1;//estado activado
            }
            else//si estoy buscando pareja
            {
                if (id != wordid)//si la pareja que encontré no es como yo, pierdo -- si la palabra word es diferente a icon
                {
                    lose.SetActive(true);
                    buttons[iconid].GetComponent<ButtonScript>().state = 3;
                }
                else if (id == wordid)//si la pareja que encontré sí es, gano
                {
                    win.SetActive(true);
                    if (particulas.isPlaying)
                    {
                        particulas.Stop();
                        celebracion_sonido.Stop();
                    }
                    particulas.Play();
                    celebracion_sonido.Play();
                    buttons[iconid].GetComponent<ButtonScript>().state = 2;
                    textButtons[iconid].GetComponent<ButtonScript>().state = 2;
                }
            }
        }
    }
    public void reiniciarBotones()
    {
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].GetComponent<ButtonScript>().state = 0;
        }
        for (int i = 0; i < buttons.Length; i++)
        {
            textButtons[i].GetComponent<ButtonScript>().state = 0;
        }
    }
    void ButtonMethod(int buttonid)
    {
        if (textButtons[buttonid].GetComponent<ButtonScript>().state != 2)//si no me lo gasté
        {
            if (!firstSelected)//no busco pareja aún
            {
                id = buttonid;//seteo mi id porque no estoy buscando pareja
                firstSelected = true;//activo mi búsqueda de pareja
                textButtons[buttonid].GetComponent<ButtonScript>().state = 1;//estado activado
            }
            else//si estoy buscando pareja
            {
                if (id != wordid)//si la pareja que encontré no es como yo, pierdo
                {
                    lose.SetActive(true);
                    textButtons[buttonid].GetComponent<ButtonScript>().state = 3;
                }
                else if (id == wordid)//si la pareja que encontré sí es, gano
                {
                    win.SetActive(true);
                    if (particulas.isPlaying)
                    {
                        particulas.Stop();
                        celebracion_sonido.Stop();
                    }
                    particulas.Play();
                    celebracion_sonido.Play();
                    buttons[buttonid].GetComponent<ButtonScript>().state = 2;
                    textButtons[buttonid].GetComponent<ButtonScript>().state = 2;
                }
            }
        }
    }

    public void Buttons(string type)
    {
        switch (type)
        {
            case "win"://(ponerle al win una pantalla de bloqueo para que no se pongan a pendejear)
                firstSelected = false;
                win.SetActive(false);
                id = -1;
                wordid = -1;
                endcounter++;
                break;
            case "lose":
                if (id != -1)
                {
                    buttons[id].GetComponent<ButtonScript>().state = 0;
                }
                if (wordid != -1)
                {
                    textButtons[wordid].GetComponent<ButtonScript>().state = 0;
                }
                lose.SetActive(false);
                id = -1;
                wordid = -1;
                firstSelected = false;
                break;
            case "bookicon":
                id = 0;//asigno mi id al botón actual
                IconMethod(id);//lo pongo en mi función para comparar
                break;
            case "bookword":
                wordid = 0;
                ButtonMethod(wordid);
                break;
            case "pencilcaseicon":
                id = 1;
                IconMethod(id);
                break;
            case "pencilcaseword":
                wordid = 1;
                ButtonMethod(wordid);
                break;
            case "pencilicon":
                id = 2;
                IconMethod(id);
                break;
            case "pencilword":
                wordid = 2;
                ButtonMethod(wordid);
                break;
            case "rulericon":
                id = 3;
                IconMethod(id);
                break;
            case "rulerword":
                wordid = 3;
                ButtonMethod(wordid);
                break;
            case "chalkicon":
                id = 4;
                IconMethod(id);
                break;
            case "chalkword":
                wordid = 4;
                ButtonMethod(wordid);
                break;
            case "bagicon":
                id = 5;
                IconMethod(id);
                break;
            case "bagword":
                wordid = 5;
                ButtonMethod(wordid);
                break;
            case "play":
                gameover.SetActive(false);
                break;
            case "close":
                reiniciarBotones();
                endcounter = 0;
                gameover.SetActive(true);
                container.SetActive(false);                
                break;
        }
    }
}
