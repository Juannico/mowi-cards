﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonScript : MonoBehaviour
{
    public GameObject star;
    public Sprite disabled, selected, right, wrong;
    public Game2Script manager;
    [HideInInspector] public int state;//0, 1, 2, 3 iguales a los sprites
    // Start is called before the first frame update
    void Start()
    {
        state = 0;//desactivado
        star.GetComponent<Image>().sprite = disabled;
    }

    // Update is called once per frame
    void Update()
    {
        if (state == 1)
        {
            star.GetComponent<Image>().sprite = selected;
        }
        else if (state == 2)
        {
            star.GetComponent<Image>().sprite = right;
        }
        else if (state == 0)
        {
            star.GetComponent<Image>().sprite = disabled;
        }
        else if (state == 3)
        {
            star.GetComponent<Image>().sprite = wrong;
        }
    }
}
