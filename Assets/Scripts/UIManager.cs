﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public GameObject instrucciones;
    public GameObject cargando;
    public GameObject principal;
    public TutorialSystem tutorial;
    private void Start()
    {
        instrucciones.SetActive(false);
        cargando.SetActive(false);
        lanzarTuto();
    }
    public void lanzarTuto()
    {
        List<string> lista_instrucciones = new List<string>();
        lista_instrucciones.Add("¡Hola! Bienvenido a Full English");
        lista_instrucciones.Add("Presiona el elefante para ver las tarjetas");
        lista_instrucciones.Add("También juega la ruleta");
        tutorial.actualizarListaInstrucciones(lista_instrucciones);
        tutorial.abrirVentana();
    }
    public void Botones (string tipo)
    {
        switch (tipo)
        {
            case "inicio":
                cargando.SetActive(true);
                StartCoroutine(Loading(1));
                break;
            case "salir":
                Application.Quit();
                break;
            case "instrucciones":
                principal.SetActive(false);
                instrucciones.SetActive(true);
                break;
            case "closeinst":
                instrucciones.SetActive(false);
                principal.SetActive(true);
                break;
            case "roulette":
                cargando.SetActive(true);
                StartCoroutine(Loading(2));
                break;
            case "books":
                StartCoroutine(Loading(3));
                break;
            case "presentation":
                cargando.SetActive(true);
                StartCoroutine(Loading(3));
                break;
        }
    }

    IEnumerator Loading(int sceneNum)
    {
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene(sceneNum);
    }
}
