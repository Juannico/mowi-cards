﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class PresentationTracker 
{
    public GameObject model;
    public MeshRenderer[] myMeshes;
    public SkinnedMeshRenderer[] mySkinned;
    // Start is called before the first frame update
    void Start()
    {
        model.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
            foreach (MeshRenderer mesh in model.GetComponentsInChildren<MeshRenderer>())
            {
                mesh.enabled = true;
            }
            foreach (SkinnedMeshRenderer skinmesh in model.GetComponentsInChildren<SkinnedMeshRenderer>())
            {
                skinmesh.enabled = true;
            }
    }

    public void TargetLost()
    {
        model.SetActive(false);
        for (int i = 0; i < myMeshes.Length; i++)
        {
            myMeshes[i].enabled = false;
        }
        for (int i = 0; i < mySkinned.Length; i++)
        {
            mySkinned[i].enabled = false;
        }
    }

    public void TargetFound()
    {
        model.SetActive(true);
        for (int i = 0; i < myMeshes.Length; i++)
        {
            myMeshes[i].enabled = true;
        }
        for (int i = 0; i < mySkinned.Length; i++)
        {
            mySkinned[i].enabled = true;
        }
    }
}
