﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Vuforia;

public class VuforiaDialogue : MonoBehaviour
{
    //VuforiaTrackManager manager;
    public HUDManager manager;
    //public GameObject dialoguebutton;
    VuforiaTrackManager mytrack;
    [HideInInspector] public bool thisMarkerAffecting;
    public GameObject model;
    // Start is called before the first frame update
    void Start()
    {
        thisMarkerAffecting = false;
        //dialoguebutton.SetActive(false);
        mytrack = GetComponent<VuforiaTrackManager>();
    }

    // Update is called once per frame
    void Update()
    {
        //el botón solo se debe activar si uno de los diálogos se activa, si otro marcador se activa, no
        if (manager.isActive /*&& mytrack.state == 0*/ && !VuforiaTrackManager.close && !thisMarkerAffecting && gameObject.CompareTag("Dialogue") && model.activeSelf)
        {
            //Debug.Log("button on");
            thisMarkerAffecting = true;
            //si el manager dice que hay un marcador activo y mi estado es inicio/fin y no he cerrado el UI
            //dialoguebutton.SetActive(true);
        }
        else if (manager.isActive && VuforiaTrackManager.close && thisMarkerAffecting)
        {
            //Debug.Log("button off");
            thisMarkerAffecting = false;
            //si el ui se ah cerrado y no hay marcador activo
            //dialoguebutton.SetActive(false);
        }
    }

}
